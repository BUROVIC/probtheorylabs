﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab4
{
	public partial class FormLab4 : Form
	{
		public FormLab4()
		{
			InitializeComponent();
			tableLayoutPanel1.ColumnCount = 0;
			tableLayoutPanel1.RowCount = 2;

			tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
			tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
			tableLayoutPanel1.ColumnStyles.Clear();

			checkBoxDistr.Checked = true;
		}

		Random rnd = new Random();

		void calculateResult()
		{
			labelError.Text = "";

			try
			{
				//Геометрическое распределение
				if (checkBoxDistr.Checked)
				{
					tableLayoutPanel1.Controls.Clear();
					tableLayoutPanel1.ColumnStyles.Clear();
					tableLayoutPanel1.ColumnCount = 0;

					var p = float.Parse(textBoxP.Text);
					var q = 1 - p;

					for (int m = 1; m <= Convert.ToInt32(textBoxN.Text); m++)
					{
						var count = tableLayoutPanel1.ColumnCount += 1;
						tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));

						tableLayoutPanel1.Controls.Add(new TextBox() { Text = m.ToString(), ReadOnly = true }, count - 1, 0);
						tableLayoutPanel1.Controls.Add(new TextBox() { Text = String.Format("{0:F10}", (p * Math.Pow(q, m - 1))), ReadOnly = true }, count - 1, 1);
					}
				}

				Dictionary<float, float> points = new Dictionary<float, float>();

				for (int i = 0; i < tableLayoutPanel1.ColumnCount; i++)
				{
					points[float.Parse(tableLayoutPanel1.GetControlFromPosition(i, 0).Text)] = float.Parse(tableLayoutPanel1.GetControlFromPosition(i, 1).Text);
				}

				//Проверка  правильности составленного ряда
				float e = 0.01f; //Погрешность
				if ((!checkBoxDistr.Checked && (points.Sum(x => x.Value) < (1 + e) && points.Sum(x => x.Value) < (1 - e))) || (points.Sum(x => x.Value) > (1 + e)))
				{
					labelError.Text = "Error: сумма вероятностей членов ряда " + (checkBoxDistr.Checked ? "больше единицы ": "не равна единице");
				}

				var Mx = points.Sum(x => x.Key * x.Value);
				labelMx.Text = " = " + Mx.ToString("0.00");

				var Dx = points.Sum(x => Math.Pow(x.Key, 2) * x.Value) - Math.Pow(Mx, 2);
				labelDx.Text = " = " + Dx.ToString("0.00");

				var sqrD = Math.Sqrt(Dx);
				labelSqrD.Text = " = " + sqrD.ToString("0.00");

				//Поиск моды
				labelM.Text = "= " + string.Join(", ", 
					points.Where(x => x.Value == points.OrderByDescending(m => m.Value).First().Value).
					Select(x => x.Key).ToArray());

				chart1.Series["F(x)"].Points.Clear();
				chart2.Series["p(x)"].Points.Clear();
				foreach (var point in points)
				{
					//Построение графика функции распределения
					chart1.Series["F(x)"].Points.AddXY(point.Key, point.Value +
						points.Where(x => x.Key < point.Key).Sum(x => x.Value));

					//Построение многоугольника распределения
					chart2.Series["p(x)"].Points.AddXY(point.Key, point.Value);
				}

				chart1.Refresh();
				chart2.Refresh();
			}
			catch
			{
				labelError.Text = "Error: Данные введены некорректно";
			}
		}

		private void textBox_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)
				&& e.KeyChar != ',' && e.KeyChar != ' ' && e.KeyChar != ';' && e.KeyChar != '-')
			{
				e.Handled = true;
			}
		}

		private void textBox_TextChanged(object sender, EventArgs e)
		{
			calculateResult();
		}

		private void buttonRemoveVal_Click(object sender, EventArgs e)
		{
			var panel = tableLayoutPanel1;
			var Colindex = panel.ColumnCount - 1;

			if (Colindex == 0)
			{
				return;
			}
			for (int i = 0; i < panel.ColumnCount; i++)
			{
				var control = panel.GetControlFromPosition(Colindex, i);
				panel.Controls.Remove(control);
			}
			for (int i = Colindex + 1; i < panel.ColumnCount; i++)
			{
				for (int j = 0; j < panel.ColumnCount; j++)
				{
					var control = panel.GetControlFromPosition(j, i);
					if (control != null)
					{
						panel.SetColumn(control, i - 1);
					}
				}
			}
			panel.ColumnStyles.RemoveAt(panel.ColumnCount - 1);
			panel.ColumnCount--;

			calculateResult();
		}

		private void buttonAddVal_Click(object sender, EventArgs e)
		{
			var count = tableLayoutPanel1.ColumnCount += 1;
			tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));

			var val = new TextBox() { Text = rnd.Next(-100, 100).ToString() };
			val.TextChanged += new EventHandler(textBox_TextChanged);
			val.KeyPress += new KeyPressEventHandler(textBox_KeyPress);
			tableLayoutPanel1.Controls.Add(val, count - 1, 0);

			var prob = new TextBox() { Text = (Convert.ToDouble(rnd.Next(0, 100)) / 100).ToString() };
			prob.TextChanged += new EventHandler(textBox_TextChanged);
			prob.KeyPress += new KeyPressEventHandler(textBox_KeyPress);
			tableLayoutPanel1.Controls.Add(prob, count - 1, 1);

			calculateResult();
		}

		private void chart1_PrePaint(object sender, ChartPaintEventArgs e)
		{
			chart1.Annotations.Clear();
			DataPoint previous = null;

			var points = chart1.Series["F(x)"].Points.OrderByDescending(p => p.XValue);

			labelFx.Text = "F(x) = { ";
			foreach (var point in points)
			{
				labelFx.Text += point.YValues[0].ToString("0.00") + " при " + (previous != null ? point.XValue + " < x < " + previous.XValue : "x > " + point.XValue) + "\n";
				ArrowAnnotation arrow = new ArrowAnnotation();
				arrow.AnchorDataPoint = point;
				arrow.Height = 0;
				arrow.ArrowStyle = ArrowStyle.Simple;
				arrow.LineWidth = 1;
				arrow.ArrowSize = 3;
				arrow.Width = previous != null ? chart1.ChartAreas[0].AxisX.ValueToPosition(previous.XValue) 
					- chart1.ChartAreas[0].AxisX.ValueToPosition(point.XValue) 
					: 10;

				chart1.Annotations.Add(arrow);

				previous = point;
			}
			labelFx.Text += "0 при x <= " + (points.Any() ? points.Last().XValue : 0) + " }";
		}

		private void checkBoxDistr_CheckedChanged(object sender, EventArgs e)
		{
			var chk = (sender as CheckBox).Checked;

			calculateResult();

			for (int i = 0; i < tableLayoutPanel1.ColumnCount; i++)
			{
				for (int j = 0; j <= 1; j++)
				{
					(tableLayoutPanel1.GetControlFromPosition(i, j) as TextBox).ReadOnly = chk;
					if (chk)
					{
						tableLayoutPanel1.GetControlFromPosition(i, j).TextChanged -= new EventHandler(textBox_TextChanged);
						tableLayoutPanel1.GetControlFromPosition(i, j).KeyPress -= new KeyPressEventHandler(textBox_KeyPress);
					}
					else
					{
						tableLayoutPanel1.GetControlFromPosition(i, j).TextChanged += new EventHandler(textBox_TextChanged);
						tableLayoutPanel1.GetControlFromPosition(i, j).KeyPress += new KeyPressEventHandler(textBox_KeyPress);
					}
				}
			}

			buttonAddVal.Enabled = !chk;
			buttonRemoveVal.Enabled = !chk;

			textBoxN.Enabled = chk;
			textBoxP.Enabled = chk;
		}
	}
}
