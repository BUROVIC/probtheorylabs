﻿namespace Lab4
{
	partial class FormLab4
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLab4));
			this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.buttonRemoveVal = new System.Windows.Forms.Button();
			this.buttonAddVal = new System.Windows.Forms.Button();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.labelFx = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.pictureBox3 = new System.Windows.Forms.PictureBox();
			this.labelMx = new System.Windows.Forms.Label();
			this.labelDx = new System.Windows.Forms.Label();
			this.labelSqrD = new System.Windows.Forms.Label();
			this.checkBoxDistr = new System.Windows.Forms.CheckBox();
			this.textBoxN = new System.Windows.Forms.TextBox();
			this.textBoxP = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.labelError = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.pictureBox4 = new System.Windows.Forms.PictureBox();
			this.labelM = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
			this.SuspendLayout();
			// 
			// chart1
			// 
			chartArea5.Name = "ChartArea1";
			this.chart1.ChartAreas.Add(chartArea5);
			legend5.Name = "Legend1";
			this.chart1.Legends.Add(legend5);
			this.chart1.Location = new System.Drawing.Point(11, 203);
			this.chart1.Name = "chart1";
			series5.BorderWidth = 2;
			series5.ChartArea = "ChartArea1";
			series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
			series5.Color = System.Drawing.Color.Blue;
			series5.Legend = "Legend1";
			series5.Name = "F(x)";
			series5.YValuesPerPoint = 2;
			this.chart1.Series.Add(series5);
			this.chart1.Size = new System.Drawing.Size(492, 300);
			this.chart1.TabIndex = 1;
			this.chart1.Text = "chart1";
			this.chart1.PrePaint += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ChartPaintEventArgs>(this.chart1_PrePaint);
			// 
			// buttonRemoveVal
			// 
			this.buttonRemoveVal.Location = new System.Drawing.Point(13, 13);
			this.buttonRemoveVal.Name = "buttonRemoveVal";
			this.buttonRemoveVal.Size = new System.Drawing.Size(23, 23);
			this.buttonRemoveVal.TabIndex = 2;
			this.buttonRemoveVal.Text = "-";
			this.buttonRemoveVal.UseVisualStyleBackColor = true;
			this.buttonRemoveVal.Click += new System.EventHandler(this.buttonRemoveVal_Click);
			// 
			// buttonAddVal
			// 
			this.buttonAddVal.Location = new System.Drawing.Point(42, 13);
			this.buttonAddVal.Name = "buttonAddVal";
			this.buttonAddVal.Size = new System.Drawing.Size(23, 23);
			this.buttonAddVal.TabIndex = 3;
			this.buttonAddVal.Text = "+";
			this.buttonAddVal.UseVisualStyleBackColor = true;
			this.buttonAddVal.Click += new System.EventHandler(this.buttonAddVal_Click);
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.AutoScroll = true;
			this.tableLayoutPanel1.AutoScrollMinSize = new System.Drawing.Size(99, 1);
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(53, 42);
			this.tableLayoutPanel1.MaximumSize = new System.Drawing.Size(600, 500);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(554, 91);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// chart2
			// 
			chartArea6.Name = "ChartArea1";
			this.chart2.ChartAreas.Add(chartArea6);
			legend6.Name = "Legend1";
			this.chart2.Legends.Add(legend6);
			this.chart2.Location = new System.Drawing.Point(523, 203);
			this.chart2.Name = "chart2";
			series6.BorderWidth = 2;
			series6.ChartArea = "ChartArea1";
			series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series6.Legend = "Legend1";
			series6.Name = "p(x)";
			this.chart2.Series.Add(series6);
			this.chart2.Size = new System.Drawing.Size(492, 300);
			this.chart2.TabIndex = 4;
			this.chart2.Text = "chart2";
			// 
			// labelFx
			// 
			this.labelFx.AutoSize = true;
			this.labelFx.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelFx.Location = new System.Drawing.Point(24, 515);
			this.labelFx.Name = "labelFx";
			this.labelFx.Size = new System.Drawing.Size(40, 16);
			this.labelFx.TabIndex = 5;
			this.labelFx.Text = "F(x) =";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(523, 541);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(140, 50);
			this.pictureBox1.TabIndex = 6;
			this.pictureBox1.TabStop = false;
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(523, 611);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(218, 45);
			this.pictureBox2.TabIndex = 7;
			this.pictureBox2.TabStop = false;
			// 
			// pictureBox3
			// 
			this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
			this.pictureBox3.Location = new System.Drawing.Point(523, 671);
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.Size = new System.Drawing.Size(140, 50);
			this.pictureBox3.TabIndex = 8;
			this.pictureBox3.TabStop = false;
			// 
			// labelMx
			// 
			this.labelMx.AutoSize = true;
			this.labelMx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelMx.Location = new System.Drawing.Point(669, 556);
			this.labelMx.Name = "labelMx";
			this.labelMx.Size = new System.Drawing.Size(51, 20);
			this.labelMx.TabIndex = 9;
			this.labelMx.Text = "label1";
			// 
			// labelDx
			// 
			this.labelDx.AutoSize = true;
			this.labelDx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelDx.Location = new System.Drawing.Point(747, 623);
			this.labelDx.Name = "labelDx";
			this.labelDx.Size = new System.Drawing.Size(51, 20);
			this.labelDx.TabIndex = 10;
			this.labelDx.Text = "label2";
			// 
			// labelSqrD
			// 
			this.labelSqrD.AutoSize = true;
			this.labelSqrD.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelSqrD.Location = new System.Drawing.Point(669, 685);
			this.labelSqrD.Name = "labelSqrD";
			this.labelSqrD.Size = new System.Drawing.Size(51, 20);
			this.labelSqrD.TabIndex = 11;
			this.labelSqrD.Text = "label3";
			// 
			// checkBoxDistr
			// 
			this.checkBoxDistr.AutoSize = true;
			this.checkBoxDistr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.checkBoxDistr.Location = new System.Drawing.Point(652, 19);
			this.checkBoxDistr.Name = "checkBoxDistr";
			this.checkBoxDistr.Size = new System.Drawing.Size(277, 24);
			this.checkBoxDistr.TabIndex = 12;
			this.checkBoxDistr.Text = "Геометрическое распределение";
			this.checkBoxDistr.UseVisualStyleBackColor = true;
			this.checkBoxDistr.CheckedChanged += new System.EventHandler(this.checkBoxDistr_CheckedChanged);
			// 
			// textBoxN
			// 
			this.textBoxN.Location = new System.Drawing.Point(685, 49);
			this.textBoxN.Name = "textBoxN";
			this.textBoxN.Size = new System.Drawing.Size(37, 20);
			this.textBoxN.TabIndex = 13;
			this.textBoxN.Text = "4";
			this.textBoxN.TextChanged += new System.EventHandler(this.textBox_TextChanged);
			this.textBoxN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
			// 
			// textBoxP
			// 
			this.textBoxP.Location = new System.Drawing.Point(685, 75);
			this.textBoxP.Name = "textBoxP";
			this.textBoxP.Size = new System.Drawing.Size(37, 20);
			this.textBoxP.TabIndex = 14;
			this.textBoxP.Text = "0.7";
			this.textBoxP.TextChanged += new System.EventHandler(this.textBox_TextChanged);
			this.textBoxP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(648, 49);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(31, 20);
			this.label1.TabIndex = 16;
			this.label1.Text = "n =";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label2.Location = new System.Drawing.Point(648, 75);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(31, 20);
			this.label2.TabIndex = 17;
			this.label2.Text = "p =";
			// 
			// labelError
			// 
			this.labelError.AutoSize = true;
			this.labelError.Font = new System.Drawing.Font("Moire", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelError.ForeColor = System.Drawing.Color.Red;
			this.labelError.Location = new System.Drawing.Point(10, 146);
			this.labelError.Name = "labelError";
			this.labelError.Size = new System.Drawing.Size(54, 19);
			this.labelError.TabIndex = 18;
			this.labelError.Text = "Error";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label3.Location = new System.Drawing.Point(8, 181);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(223, 16);
			this.label3.TabIndex = 19;
			this.label3.Text = "График функции распределения";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label4.Location = new System.Drawing.Point(527, 181);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(214, 16);
			this.label4.TabIndex = 20;
			this.label4.Text = "Многоугольник распределения";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label5.Location = new System.Drawing.Point(81, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(129, 20);
			this.label5.TabIndex = 21;
			this.label5.Text = "Ряд распределения";
			this.label5.UseCompatibleTextRendering = true;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label6.Location = new System.Drawing.Point(520, 515);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(181, 16);
			this.label6.TabIndex = 22;
			this.label6.Text = "Числовые характеристики";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label7.Location = new System.Drawing.Point(16, 42);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(31, 25);
			this.label7.TabIndex = 23;
			this.label7.Text = "Xi";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label8.Location = new System.Drawing.Point(16, 86);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(31, 25);
			this.label8.TabIndex = 24;
			this.label8.Text = "Pi";
			// 
			// pictureBox4
			// 
			this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
			this.pictureBox4.Location = new System.Drawing.Point(523, 730);
			this.pictureBox4.Name = "pictureBox4";
			this.pictureBox4.Size = new System.Drawing.Size(37, 28);
			this.pictureBox4.TabIndex = 25;
			this.pictureBox4.TabStop = false;
			// 
			// labelM
			// 
			this.labelM.AutoSize = true;
			this.labelM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelM.Location = new System.Drawing.Point(566, 738);
			this.labelM.Name = "labelM";
			this.labelM.Size = new System.Drawing.Size(51, 20);
			this.labelM.TabIndex = 26;
			this.labelM.Text = "label9";
			// 
			// FormLab4
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.ClientSize = new System.Drawing.Size(1054, 791);
			this.Controls.Add(this.labelM);
			this.Controls.Add(this.pictureBox4);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.labelError);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBoxP);
			this.Controls.Add(this.textBoxN);
			this.Controls.Add(this.checkBoxDistr);
			this.Controls.Add(this.labelSqrD);
			this.Controls.Add(this.labelDx);
			this.Controls.Add(this.labelMx);
			this.Controls.Add(this.pictureBox3);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.labelFx);
			this.Controls.Add(this.chart2);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.buttonAddVal);
			this.Controls.Add(this.buttonRemoveVal);
			this.Controls.Add(this.chart1);
			this.Name = "FormLab4";
			this.Text = "Лабораторная №4";
			((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
		private System.Windows.Forms.Button buttonRemoveVal;
		private System.Windows.Forms.Button buttonAddVal;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
		private System.Windows.Forms.Label labelFx;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.PictureBox pictureBox3;
		private System.Windows.Forms.Label labelMx;
		private System.Windows.Forms.Label labelDx;
		private System.Windows.Forms.Label labelSqrD;
		private System.Windows.Forms.CheckBox checkBoxDistr;
		private System.Windows.Forms.TextBox textBoxN;
		private System.Windows.Forms.TextBox textBoxP;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label labelError;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.PictureBox pictureBox4;
		private System.Windows.Forms.Label labelM;
	}
}

