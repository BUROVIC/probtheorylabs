﻿using System.Windows.Forms;

namespace Lab1
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.comboBoxFormula = new System.Windows.Forms.ComboBox();
			this.checkBoxWithReps = new System.Windows.Forms.CheckBox();
			this.labelM = new System.Windows.Forms.Label();
			this.labelN = new System.Windows.Forms.Label();
			this.labelResult = new System.Windows.Forms.Label();
			this.labelResultIs = new System.Windows.Forms.Label();
			this.textBoxM = new System.Windows.Forms.TextBox();
			this.textBoxN = new System.Windows.Forms.TextBox();
			this.checkBoxTask = new System.Windows.Forms.CheckBox();
			this.comboBoxTask = new System.Windows.Forms.ComboBox();
			this.labelTask = new System.Windows.Forms.Label();
			this.labelResultTask = new System.Windows.Forms.Label();
			this.pictureBoxFormula4 = new System.Windows.Forms.PictureBox();
			this.pictureBoxFormula3 = new System.Windows.Forms.PictureBox();
			this.pictureBoxFormula2 = new System.Windows.Forms.PictureBox();
			this.pictureBoxFormula1 = new System.Windows.Forms.PictureBox();
			this.pictureBoxTaskFormula = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxFormula4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxFormula3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxFormula2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxFormula1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxTaskFormula)).BeginInit();
			this.SuspendLayout();
			// 
			// comboBoxFormula
			// 
			this.comboBoxFormula.BackColor = System.Drawing.SystemColors.Window;
			this.comboBoxFormula.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxFormula.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.comboBoxFormula.FormattingEnabled = true;
			this.comboBoxFormula.Items.AddRange(new object[] {
            "Сочетания",
            "Размещения"});
			this.comboBoxFormula.Location = new System.Drawing.Point(12, 12);
			this.comboBoxFormula.Name = "comboBoxFormula";
			this.comboBoxFormula.Size = new System.Drawing.Size(174, 26);
			this.comboBoxFormula.TabIndex = 0;
			this.comboBoxFormula.SelectedIndexChanged += new System.EventHandler(this.comboBoxFormula_SelectedIndexChanged);
			// 
			// checkBoxWithReps
			// 
			this.checkBoxWithReps.AutoSize = true;
			this.checkBoxWithReps.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.checkBoxWithReps.Location = new System.Drawing.Point(211, 31);
			this.checkBoxWithReps.Name = "checkBoxWithReps";
			this.checkBoxWithReps.Size = new System.Drawing.Size(134, 20);
			this.checkBoxWithReps.TabIndex = 1;
			this.checkBoxWithReps.Text = "С повторениями";
			this.checkBoxWithReps.UseVisualStyleBackColor = true;
			this.checkBoxWithReps.CheckedChanged += new System.EventHandler(this.checkBoxWithReps_CheckedChanged);
			// 
			// labelM
			// 
			this.labelM.AutoSize = true;
			this.labelM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelM.Location = new System.Drawing.Point(207, 54);
			this.labelM.Name = "labelM";
			this.labelM.Size = new System.Drawing.Size(35, 20);
			this.labelM.TabIndex = 2;
			this.labelM.Text = "m =";
			// 
			// labelN
			// 
			this.labelN.AutoSize = true;
			this.labelN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelN.Location = new System.Drawing.Point(207, 83);
			this.labelN.Name = "labelN";
			this.labelN.Size = new System.Drawing.Size(31, 20);
			this.labelN.TabIndex = 3;
			this.labelN.Text = "n =";
			// 
			// labelResult
			// 
			this.labelResult.AutoSize = true;
			this.labelResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelResult.Location = new System.Drawing.Point(8, 146);
			this.labelResult.Name = "labelResult";
			this.labelResult.Size = new System.Drawing.Size(18, 20);
			this.labelResult.TabIndex = 9;
			this.labelResult.Text = "0";
			// 
			// labelResultIs
			// 
			this.labelResultIs.AutoSize = true;
			this.labelResultIs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelResultIs.Location = new System.Drawing.Point(12, 117);
			this.labelResultIs.Name = "labelResultIs";
			this.labelResultIs.Size = new System.Drawing.Size(93, 20);
			this.labelResultIs.TabIndex = 10;
			this.labelResultIs.Text = "Результат:";
			// 
			// textBoxM
			// 
			this.textBoxM.Location = new System.Drawing.Point(248, 54);
			this.textBoxM.Name = "textBoxM";
			this.textBoxM.Size = new System.Drawing.Size(100, 20);
			this.textBoxM.TabIndex = 11;
			this.textBoxM.Text = "0";
			this.textBoxM.TextChanged += new System.EventHandler(this.textBoxM_TextChanged);
			this.textBoxM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxM_KeyPress);
			// 
			// textBoxN
			// 
			this.textBoxN.Location = new System.Drawing.Point(248, 83);
			this.textBoxN.Name = "textBoxN";
			this.textBoxN.Size = new System.Drawing.Size(100, 20);
			this.textBoxN.TabIndex = 12;
			this.textBoxN.Text = "0";
			this.textBoxN.TextChanged += new System.EventHandler(this.textBoxN_TextChanged);
			this.textBoxN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxN_KeyPress);
			// 
			// checkBoxTask
			// 
			this.checkBoxTask.AutoSize = true;
			this.checkBoxTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.checkBoxTask.Location = new System.Drawing.Point(211, 8);
			this.checkBoxTask.Name = "checkBoxTask";
			this.checkBoxTask.Size = new System.Drawing.Size(95, 20);
			this.checkBoxTask.TabIndex = 17;
			this.checkBoxTask.Text = "С задачей";
			this.checkBoxTask.UseVisualStyleBackColor = true;
			this.checkBoxTask.CheckedChanged += new System.EventHandler(this.checkBoxTask_CheckedChanged);
			// 
			// comboBoxTask
			// 
			this.comboBoxTask.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.comboBoxTask.FormattingEnabled = true;
			this.comboBoxTask.Items.AddRange(new object[] {
            "Выбор дежурного",
            "Корзина с игрушками",
            "Ваза с цветами",
            "Решение задач"});
			this.comboBoxTask.Location = new System.Drawing.Point(399, 8);
			this.comboBoxTask.Name = "comboBoxTask";
			this.comboBoxTask.Size = new System.Drawing.Size(176, 26);
			this.comboBoxTask.TabIndex = 18;
			this.comboBoxTask.Visible = false;
			this.comboBoxTask.SelectedIndexChanged += new System.EventHandler(this.comboBoxTask_SelectedIndexChanged);
			// 
			// labelTask
			// 
			this.labelTask.AutoSize = true;
			this.labelTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelTask.Location = new System.Drawing.Point(396, 53);
			this.labelTask.Name = "labelTask";
			this.labelTask.Size = new System.Drawing.Size(0, 16);
			this.labelTask.TabIndex = 19;
			this.labelTask.Visible = false;
			// 
			// labelResultTask
			// 
			this.labelResultTask.AutoSize = true;
			this.labelResultTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelResultTask.Location = new System.Drawing.Point(8, 146);
			this.labelResultTask.Name = "labelResultTask";
			this.labelResultTask.Size = new System.Drawing.Size(18, 20);
			this.labelResultTask.TabIndex = 20;
			this.labelResultTask.Text = "1";
			this.labelResultTask.Visible = false;
			// 
			// pictureBoxFormula4
			// 
			this.pictureBoxFormula4.Image = global::Lab1.Properties.Resources.Formula4;
			this.pictureBoxFormula4.Location = new System.Drawing.Point(12, 53);
			this.pictureBoxFormula4.Name = "pictureBoxFormula4";
			this.pictureBoxFormula4.Size = new System.Drawing.Size(88, 31);
			this.pictureBoxFormula4.TabIndex = 16;
			this.pictureBoxFormula4.TabStop = false;
			this.pictureBoxFormula4.Visible = false;
			// 
			// pictureBoxFormula3
			// 
			this.pictureBoxFormula3.Image = global::Lab1.Properties.Resources.Formula3;
			this.pictureBoxFormula3.Location = new System.Drawing.Point(13, 53);
			this.pictureBoxFormula3.Name = "pictureBoxFormula3";
			this.pictureBoxFormula3.Size = new System.Drawing.Size(122, 50);
			this.pictureBoxFormula3.TabIndex = 15;
			this.pictureBoxFormula3.TabStop = false;
			this.pictureBoxFormula3.Visible = false;
			// 
			// pictureBoxFormula2
			// 
			this.pictureBoxFormula2.Image = global::Lab1.Properties.Resources.Formula2;
			this.pictureBoxFormula2.Location = new System.Drawing.Point(13, 53);
			this.pictureBoxFormula2.Name = "pictureBoxFormula2";
			this.pictureBoxFormula2.Size = new System.Drawing.Size(116, 34);
			this.pictureBoxFormula2.TabIndex = 14;
			this.pictureBoxFormula2.TabStop = false;
			this.pictureBoxFormula2.Visible = false;
			// 
			// pictureBoxFormula1
			// 
			this.pictureBoxFormula1.Image = global::Lab1.Properties.Resources.Formula1;
			this.pictureBoxFormula1.Location = new System.Drawing.Point(13, 53);
			this.pictureBoxFormula1.Name = "pictureBoxFormula1";
			this.pictureBoxFormula1.Size = new System.Drawing.Size(137, 50);
			this.pictureBoxFormula1.TabIndex = 13;
			this.pictureBoxFormula1.TabStop = false;
			this.pictureBoxFormula1.Visible = false;
			// 
			// pictureBoxTaskFormula
			// 
			this.pictureBoxTaskFormula.Location = new System.Drawing.Point(587, 8);
			this.pictureBoxTaskFormula.Name = "pictureBoxTaskFormula";
			this.pictureBoxTaskFormula.Size = new System.Drawing.Size(92, 43);
			this.pictureBoxTaskFormula.TabIndex = 21;
			this.pictureBoxTaskFormula.TabStop = false;
			this.pictureBoxTaskFormula.Visible = false;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.ClientSize = new System.Drawing.Size(362, 211);
			this.Controls.Add(this.pictureBoxTaskFormula);
			this.Controls.Add(this.labelResultTask);
			this.Controls.Add(this.labelTask);
			this.Controls.Add(this.comboBoxTask);
			this.Controls.Add(this.checkBoxTask);
			this.Controls.Add(this.pictureBoxFormula4);
			this.Controls.Add(this.pictureBoxFormula3);
			this.Controls.Add(this.pictureBoxFormula2);
			this.Controls.Add(this.pictureBoxFormula1);
			this.Controls.Add(this.textBoxN);
			this.Controls.Add(this.textBoxM);
			this.Controls.Add(this.labelResultIs);
			this.Controls.Add(this.labelResult);
			this.Controls.Add(this.labelN);
			this.Controls.Add(this.labelM);
			this.Controls.Add(this.checkBoxWithReps);
			this.Controls.Add(this.comboBoxFormula);
			this.MinimumSize = new System.Drawing.Size(378, 250);
			this.Name = "Form1";
			this.Text = "Лабораторная №1";
			this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxFormula4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxFormula3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxFormula2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxFormula1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxTaskFormula)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox comboBoxFormula;
		private System.Windows.Forms.CheckBox checkBoxWithReps;
		private System.Windows.Forms.Label labelM;
		private System.Windows.Forms.Label labelN;
		private System.Windows.Forms.Label labelResult;
		private System.Windows.Forms.Label labelResultIs;
		private System.Windows.Forms.TextBox textBoxM;
		private System.Windows.Forms.TextBox textBoxN;
		private PictureBox pictureBoxFormula1;
		private PictureBox pictureBoxFormula2;
		private PictureBox pictureBoxFormula3;
		private PictureBox pictureBoxFormula4;
		private CheckBox checkBoxTask;
		private ComboBox comboBoxTask;
		private Label labelTask;
		private Label labelResultTask;
		private PictureBox pictureBoxTaskFormula;
	}
}

