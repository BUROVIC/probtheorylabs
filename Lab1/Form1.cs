﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;
using System.Resources;

namespace Lab1
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			comboBoxFormula.SelectedIndex = 0;
			comboBoxTask.SelectedIndex = 0;
			pictureBoxTaskFormula.SizeMode = PictureBoxSizeMode.StretchImage;
		}

		private void comboBoxFormula_SelectedIndexChanged(object sender, EventArgs e)
		{
			calculateResult();
		}

		private void checkBoxWithReps_CheckedChanged(object sender, EventArgs e)
		{
			calculateResult();
		}

		private void textBoxM_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
			{
				e.Handled = true;
			}
		}

		private void textBoxM_TextChanged(object sender, EventArgs e)
		{
			calculateResult();
		}

		private void textBoxN_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
			{
				e.Handled = true;
			}
		}

		private void textBoxN_TextChanged(object sender, EventArgs e)
		{
			calculateResult();
		}

		private void calculateResult()
		{
			BigInteger m;
			BigInteger n;

			BigInteger.TryParse(textBoxM.Text, out m);
			BigInteger.TryParse(textBoxN.Text, out n);

			BigInteger result = 0;

			//set all formulas invisible
			pictureBoxFormula1.Visible = false;
			pictureBoxFormula2.Visible = false;
			pictureBoxFormula3.Visible = false;
			pictureBoxFormula4.Visible = false;

			//Сочетания без повторений
			if (comboBoxFormula.SelectedIndex == 0 && !checkBoxWithReps.Checked)
			{
				pictureBoxFormula1.Visible = true;

				BigInteger NumeratorFactorial = 1;
                for (int i = 2; i <= (int)n; i++)
                {
                    NumeratorFactorial = NumeratorFactorial * i;
                }

                BigInteger DenominatorFactorial = 1;
                for (int i = 2; i <= (int)(n - m); i++)
                {
                    DenominatorFactorial = DenominatorFactorial * i;
                }

                BigInteger MDenominatorFactorial = 1;
                for (int i = 2; i <= (int)m; i++)
                {
                    MDenominatorFactorial = MDenominatorFactorial * i;
                }
                result = NumeratorFactorial / (DenominatorFactorial * MDenominatorFactorial);
			}
			//Сочетания с повторениями
			else if (comboBoxFormula.SelectedIndex == 0 && checkBoxWithReps.Checked)
			{
				pictureBoxFormula2.Visible = true;

				n = n + m - 1;
                BigInteger NumeratorFactorial = 1;
                for (int i = 2; i <= (int)n; i++)
                {
                    NumeratorFactorial = NumeratorFactorial * i;
                }

                BigInteger DenominatorFactorial = 1;
                for (int i = 2; i <= (int)(n - m); i++)
                {
                    DenominatorFactorial = DenominatorFactorial * i;
                }

                BigInteger MDenominatorFactorial = 1;
                for (int i = 2; i <= (int)m; i++)
                {
                    MDenominatorFactorial = MDenominatorFactorial * i;
                }
                result = NumeratorFactorial / (DenominatorFactorial * MDenominatorFactorial);
			}
			//Раземещения без повторений
            else if (comboBoxFormula.SelectedIndex == 1 && !checkBoxWithReps.Checked)
            {
				pictureBoxFormula3.Visible = true;

				BigInteger NumeratorFactorial = 1;
                for (int i = 2; i <= (int)n; i++)
                {
                    NumeratorFactorial = NumeratorFactorial * i;
                }

                BigInteger DenominatorFactorial = 1;
                for (int i = 2; i <= (int)(n - m); i++)
                {
                    DenominatorFactorial = DenominatorFactorial * i;
                }
                result = NumeratorFactorial / DenominatorFactorial;
            }
            //Раземещения с повторениями
            else if (comboBoxFormula.SelectedIndex == 1 && checkBoxWithReps.Checked)
            {
				pictureBoxFormula4.Visible = true;

				result = BigInteger.Pow(n, (int)m);
            }

			labelResult.Text = AppendAtPosition(result.ToString(), this.Width / 10, "\n");

			calculateTaskResult();
		}

		private void Form1_SizeChanged(object sender, EventArgs e)
		{
			labelResult.Text = AppendAtPosition(labelResult.Text.Replace("\n", ""), this.Width / 10, "\n");
		}

		private static string AppendAtPosition(string baseString, int position, string character)
		{
			var sb = new StringBuilder(baseString);
			for (int i = position; i < sb.Length; i += (position + character.Length))
				sb.Insert(i, character);
			return sb.ToString();
		}

		private void checkBoxTask_CheckedChanged(object sender, EventArgs e)
		{
			bool chk = (sender as CheckBox).Checked;

			comboBoxFormula.Enabled = !chk;
			checkBoxWithReps.Enabled = !chk;

			labelResult.Visible = !chk;

			comboBoxTask.Visible = chk;
			labelTask.Visible = chk;
			pictureBoxTaskFormula.Visible = chk;
			labelResultTask.Visible = chk;

			this.MinimumSize = chk ? new Size(930, this.Height) : new Size(378, this.Height);

			comboBoxTask_SelectedIndexChanged(comboBoxTask, new EventArgs());
		}

		private void comboBoxTask_SelectedIndexChanged(object sender, EventArgs e)
		{
			ResourceManager rm = new ResourceManager(typeof(Lab1.Properties.Resources));
			switch ((sender as ComboBox).SelectedIndex)
			{
				case 0:
					labelTask.Text = rm.GetString("Task1").Replace("\\n", "\n");
					pictureBoxTaskFormula.Image = Properties.Resources.TaskFormula1;
					comboBoxFormula.SelectedIndex = 0;
					checkBoxWithReps.Checked = false;
					break;
				case 1:
					labelTask.Text = rm.GetString("Task2").Replace("\\n", "\n");
					pictureBoxTaskFormula.Image = Properties.Resources.TaskFormula2;
					comboBoxFormula.SelectedIndex = 0;
					checkBoxWithReps.Checked = true;
					break;
				case 2:
					labelTask.Text = rm.GetString("Task3").Replace("\\n", "\n");
					pictureBoxTaskFormula.Image = Properties.Resources.TaskFormula3;
					comboBoxFormula.SelectedIndex = 1;
					checkBoxWithReps.Checked = false;
					break;
				case 3:
					labelTask.Text = rm.GetString("Task4").Replace("\\n", "\n");
					pictureBoxTaskFormula.Image = Properties.Resources.TaskFormula4;
					comboBoxFormula.SelectedIndex = 1;
					checkBoxWithReps.Checked = true;
					break;
				default:
					break;
			}

			calculateTaskResult();
		}

		private void calculateTaskResult()
		{
			BigInteger denominator;

			BigInteger numeratorFactorial = 1;
			for (int i = 2; i <= Convert.ToInt32(textBoxM.Text == "" ? "0" : textBoxM.Text); i++)
			{
				numeratorFactorial = numeratorFactorial * i;
			}

			BigInteger.TryParse(labelResult.Text.Replace("\n", ""), out denominator);

			double result = ((double)numeratorFactorial / (double)denominator);
			labelResultTask.Text = Double.IsNaN(result) || Double.IsInfinity(result) ? "Данные не входят в допустимый предел" : (result >= 1 ? "Данные введены не корректно" : result.ToString());
		}
	}
}
