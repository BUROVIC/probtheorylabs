﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
namespace Lab3
{
	public partial class FormLab3 : Form
	{
		public FormLab3()
		{
			InitializeComponent();
			comboBoxFormula.SelectedIndex = 0;

			changeBernFormula(1);
		}

		private void comboBoxFormula_SelectedIndexChanged(object sender, EventArgs e)
		{
			disableUi();
			setBernFormulasVisible(false);
			switch (comboBoxFormula.SelectedIndex)
			{
				case 0:
					pictureBoxFormula.Image = Properties.Resources.Formula1;
					setBernFormulasVisible(true);

					textBoxP.Enabled = true;
					textBoxN.Enabled = true;
					textBoxM.Enabled = true;
					textBoxM1.Enabled = true;
					textBoxM2.Enabled = true;
					break;
				case 1:
					pictureBoxFormula.Image = Properties.Resources.Formula2;

					textBoxN.Enabled = true;
					textBoxK.Enabled = true;
					textBoxMk.Enabled = true;
					textBoxPk.Enabled = true;
					break;
				case 2:
					pictureBoxFormula.Image = Properties.Resources.Formula3;

					textBoxN.Enabled = true;
					textBoxP.Enabled = true;
					textBoxM1.Enabled = true;
					textBoxM2.Enabled = true;
					break;
			}

			calculateResult();
		}

		private void disableUi()
		{
			textBoxP.Enabled = false;
			textBoxN.Enabled = false;
			textBoxK.Enabled = false;
			textBoxM.Enabled = false;
			textBoxM1.Enabled = false;
			textBoxM2.Enabled = false;
			textBoxMk.Enabled = false;
			textBoxPk.Enabled = false;
		}

		private void setBernFormulasVisible(bool visible)
		{
			pictureBoxBern1.Visible = visible;
			pictureBoxBern2.Visible = visible;
			pictureBoxBern3.Visible = visible;
			pictureBoxBern4.Visible = visible;
		}

		private void calculateResult()
		{
			float result = 0;
            float result1 = 0;
            float result2 = 0;
            float p = 0;
            float n = 0;
            float k = 0;
            float m = 0;
            float m1 = 0;
            float m2 = 0;
            float mk = 0;
            float X1 = 0;
            float X2 = 0;
            float Mfac = 1;
            float Pfac = 1;
            int Kcount = 0;
            int Kkcount = 0;
			try
			{
                n = float.Parse(textBoxN.Text);

                switch (comboBoxFormula.SelectedIndex)
                {
                    case 0:

                        //Испоьзуемые параметры: (p, n, m, m1, m2) 
                        p = float.Parse(textBoxP.Text);

                        pictureBoxFormula.Image = Properties.Resources.Formula1;
                        if (pictureBoxBern1.BorderStyle == BorderStyle.Fixed3D)
                        {
                            m = float.Parse(textBoxM.Text);

                            //TODO: НУЖНА ФОРМУЛА


                            int NumeratorFactorial = 1;
                            for (int i = 2; i <= (int)n; i++)
                            {
                                NumeratorFactorial = NumeratorFactorial * i;
                            }

                            int DenominatorFactorial = 1;
                            for (int i = 2; i <= (int)(n - m); i++)
                            {
                                DenominatorFactorial = DenominatorFactorial * i;
                            }

                            int MDenominatorFactorial = 1;
                            for (int i = 2; i <= (int)m; i++)
                            {
                                MDenominatorFactorial = MDenominatorFactorial * i;
                            }
                            result1 = NumeratorFactorial / (DenominatorFactorial * MDenominatorFactorial);
                            float q = 1 - p;
                            float x = n - m;

                            result = result1 * (float)Math.Pow(p, m) * (float)Math.Pow(q, x); // Результат вычислений
                        }
                        else if (pictureBoxBern2.BorderStyle == BorderStyle.Fixed3D)
                        {
                            m = float.Parse(textBoxM.Text);
                            float y = m - 1;

                            while (y >= 0)
                            {
                                //TODO: НУЖНА ФОРМУЛА
                                int NumeratorFactorial = 1;
                                for (int i = 2; i <= (int)n; i++)
                                {
                                    NumeratorFactorial = NumeratorFactorial * i;
                                }

                                int DenominatorFactorial = 1;
                                for (int i = 2; i <= (int)(n - y); i++)
                                {
                                    DenominatorFactorial = DenominatorFactorial * i;
                                }

                                int MDenominatorFactorial = 1;
                                for (int i = 2; i <= (int)y; i++)
                                {
                                    MDenominatorFactorial = MDenominatorFactorial * i;
                                }
                                result1 = NumeratorFactorial / (DenominatorFactorial * MDenominatorFactorial);
                                float q = 1 - p;
                                float x = n - y;

                                result2 = result1 * (float)Math.Pow(p, y) * (float)Math.Pow(q, x); // Результат вычислений
                                y--;
                                result += result2;
                            }
                        }
                        else if (pictureBoxBern3.BorderStyle == BorderStyle.Fixed3D)
                        {
                            m = float.Parse(textBoxM.Text);

                            //TODO: НУЖНА ФОРМУЛА

                            while (m <= n)
                            {
                                //TODO: НУЖНА ФОРМУЛА
                                int NumeratorFactorial = 1;
                                for (int i = 2; i <= (int)n; i++)
                                {
                                    NumeratorFactorial = NumeratorFactorial * i;
                                }

                                int DenominatorFactorial = 1;
                                for (int i = 2; i <= (int)(n - m); i++)
                                {
                                    DenominatorFactorial = DenominatorFactorial * i;
                                }

                                int MDenominatorFactorial = 1;
                                for (int i = 2; i <= (int)m; i++)
                                {
                                    MDenominatorFactorial = MDenominatorFactorial * i;
                                }
                                result1 = NumeratorFactorial / (DenominatorFactorial * MDenominatorFactorial);
                                float q = 1 - p;
                                float x = n - m;

                                result2 = result1 * (float)Math.Pow(p, m) * (float)Math.Pow(q, x); // Результат вычислений
                                m++;
                                result += result2;
                            }

                        }

						else
						{
							textBoxM.Enabled = false;
							textBoxM1.Enabled = true;
							textBoxM2.Enabled = true;

							m1 = float.Parse(textBoxM1.Text);
							m2 = float.Parse(textBoxM2.Text);

                            //Вычисление инетеграла exp(-((x^2)/2))
                             X1 = ((m1 - n * p) / Convert.ToSingle(Math.Sqrt(n * p * (1 - p)))) / Convert.ToSingle(Math.Sqrt(2));
                             X2 = ((m2 - n * p) / Convert.ToSingle(Math.Sqrt(n * p * (1 - p)))) / Convert.ToSingle(Math.Sqrt(2)); ;

                            result = (1 / Convert.ToSingle(Math.Sqrt(Math.PI * 2))) * (Convert.ToSingle(((Math.Sqrt(Math.PI)) * Erf(X2)) / (Math.Sqrt(2)) - ((Math.Sqrt(Math.PI)) * Erf(X1)) / (Math.Sqrt(2)))); // Результат вычислений

						}
						break;

					case 1:
						//Полиномиальная формула
						//Испоьзуемые параметры: (n, k, m1, m2,…, mk, p1, p2, …, pk)
						// Контроль ввода (m1 + m2 +… + mk = n);
                        
						k = float.Parse(textBoxK.Text);
                        k--;

						var Mk = getProbsMk();
						var Pk = getProbsPk();

                        Kcount = (int)k;

                        //Вычисление произведения факториалов M
                        for (int i = 0; i < k; i++)
                        {
                            Mfac *= Factorial((int)Mk[(int)Kcount]);
                            Kcount--;
                        }

                        Kcount = (int)k;
                        Kkcount = Kcount;
                        Kkcount++;

                        //Вычисление произведения факториалов P
                        for (int i = 0; i < Kkcount; i++)
                        {
                            Pfac *= (float)Math.Pow(Pk[(int)Kcount], Mk[(int)Kcount]);
                            Kcount--;
                        }
						result = (Factorial((int)n)/Mfac)*Pfac; // Результат вычислений

						break;
					case 2:

						//Интегральная теорема Муавра-Лапласа
						//Испоьзуемые параметры: (n, m1, m2, p) 

						m1 = float.Parse(textBoxM1.Text);
						m2 = float.Parse(textBoxM2.Text);

						p = float.Parse(textBoxP.Text);

                        //Вычисление инетеграла exp(-((x^2)/2))
                         X1 = ((m1 - n * p) / Convert.ToSingle(Math.Sqrt(n * p * (1 - p))))/Convert.ToSingle(Math.Sqrt(2));
                         X2 = ((m2 - n * p) / Convert.ToSingle(Math.Sqrt(n * p * (1 - p))))/Convert.ToSingle(Math.Sqrt(2));;

						result = (1/Convert.ToSingle(Math.Sqrt(Math.PI*2))) * (Convert.ToSingle(((Math.Sqrt(Math.PI)) * Erf(X2))/(Math.Sqrt(2)) - ((Math.Sqrt(Math.PI)) * Erf(X1))/(Math.Sqrt(2)))); // Результат вычислений

						break;
					default:
						break;
				}

				labelResult.Text = result.ToString();
			}
			catch (Exception)
			{
				labelResult.Text = "Ошибка ввода данных";
			}
		}

		#region Выбор формулы Бернулли
		private void changeBernFormula(int i)
		{
			foreach (var item in new List<PictureBox>
			{ pictureBoxBern1, pictureBoxBern2, pictureBoxBern3, pictureBoxBern4 }.Select((value, j) => new { j, value }))
				item.value.BorderStyle = item.j + 1 == i ? BorderStyle.Fixed3D : BorderStyle.None;

			bool isFourth = i == 4;
			textBoxM.Enabled = !isFourth;
			textBoxM1.Enabled = isFourth;
			textBoxM2.Enabled = isFourth;

			calculateResult();
		}

		private void pictureBoxBern1_Click(object sender, EventArgs e)
		{
			changeBernFormula(1);
		}

		private void pictureBoxBern2_Click(object sender, EventArgs e)
		{
			changeBernFormula(2);
		}

		private void pictureBoxBern3_Click(object sender, EventArgs e)
		{
			changeBernFormula(3);
		}

		private void pictureBoxBern4_Click(object sender, EventArgs e)
		{
			changeBernFormula(4);
		}
		#endregion

		#region Обработка ввода данных

		private void handleNumInput(KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)
				&& e.KeyChar != ',' && e.KeyChar != ' ' && e.KeyChar != ';')
			{
				e.Handled = true;
			}
		}

		private void textBoxP_KeyPress(object sender, KeyPressEventArgs e)
		{
			handleNumInput(e);
		}

		private void textBoxP_TextChanged(object sender, EventArgs e)
		{
			calculateResult();
		}

		private void textBoxN_KeyPress(object sender, KeyPressEventArgs e)
		{
			handleNumInput(e);
		}

		private void textBoxN_TextChanged(object sender, EventArgs e)
		{
			calculateResult();
		}

		private void textBoxK_KeyPress(object sender, KeyPressEventArgs e)
		{
			handleNumInput(e);
		}

		private void textBoxK_TextChanged(object sender, EventArgs e)
		{
			calculateResult();
		}

		private void textBoxM_KeyPress(object sender, KeyPressEventArgs e)
		{
			handleNumInput(e);
		}

		private void textBoxM_TextChanged(object sender, EventArgs e)
		{
			calculateResult();
		}

		private void textBoxM1_KeyPress(object sender, KeyPressEventArgs e)
		{
			handleNumInput(e);
		}

		private void textBoxM1_TextChanged(object sender, EventArgs e)
		{
			calculateResult();
		}

		private void textBoxM2_KeyPress(object sender, KeyPressEventArgs e)
		{
			handleNumInput(e);
		}

		private void textBoxM2_TextChanged(object sender, EventArgs e)
		{
			calculateResult();
		}

		private List<float> getProbsMk()
		{
			//TODO: нормальная проверка правильности заданной строки
			try
			{
				return new List<float>(textBoxMk.Text.Replace(" ", string.Empty).Split(';').Select(float.Parse).ToList());
			}
			catch (Exception)
			{
				return null;
			}

		}

		private void textBoxMk_KeyPress(object sender, KeyPressEventArgs e)
		{
			handleNumInput(e);
		}

		private void textBoxMk_TextChanged(object sender, EventArgs e)
		{
			calculateResult();
		}

		private List<float> getProbsPk()
		{
			//TODO: нормальная проверка правильности заданной строки
			try
			{
				return new List<float>(textBoxPk.Text.Replace(" ", string.Empty).Split(';').Select(float.Parse).ToList());
			}
			catch (Exception)
			{
				return null;
			}

		}

		private void textBoxPk_KeyPress(object sender, KeyPressEventArgs e)
		{
			handleNumInput(e);
		}

		private void textBoxPk_TextChanged(object sender, EventArgs e)
		{
			calculateResult();
		}

		#endregion

        #region Функция вычисления ошибок erf(x)
        static double Erf(double x)
        {
            // constants
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;
            double p = 0.3275911;

            // Save the sign of x
            int sign = 1;
            if (x < 0)
                sign = -1;
            x = Math.Abs(x);

            // A&S formula 7.1.26
            double t = 1.0 / (1.0 + p * x);
            double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);

            return sign * y;
        }
        #endregion

       //Подсчет факториала
        public int Factorial(int numb)
        {
        int res=1;
        for (int i=numb; i>1;i--)
        res*=i;
        return res;
        }
    }

}

