﻿namespace Lab3
{
	partial class FormLab3
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.comboBoxFormula = new System.Windows.Forms.ComboBox();
			this.pictureBoxFormula = new System.Windows.Forms.PictureBox();
			this.pictureBoxBern4 = new System.Windows.Forms.PictureBox();
			this.pictureBoxBern3 = new System.Windows.Forms.PictureBox();
			this.pictureBoxBern2 = new System.Windows.Forms.PictureBox();
			this.pictureBoxBern1 = new System.Windows.Forms.PictureBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.textBoxP = new System.Windows.Forms.TextBox();
			this.textBoxN = new System.Windows.Forms.TextBox();
			this.textBoxK = new System.Windows.Forms.TextBox();
			this.textBoxM = new System.Windows.Forms.TextBox();
			this.textBoxM1 = new System.Windows.Forms.TextBox();
			this.textBoxM2 = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.textBoxMk = new System.Windows.Forms.TextBox();
			this.textBoxPk = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.labelResult = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxFormula)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxBern4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxBern3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxBern2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxBern1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(13, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(79, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Формула";
			// 
			// comboBoxFormula
			// 
			this.comboBoxFormula.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxFormula.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.comboBoxFormula.FormattingEnabled = true;
			this.comboBoxFormula.Items.AddRange(new object[] {
            "Бернулли",
            "полиномиальная",
            "Муавра-Лапласа (интегральная)"});
			this.comboBoxFormula.Location = new System.Drawing.Point(98, 10);
			this.comboBoxFormula.Name = "comboBoxFormula";
			this.comboBoxFormula.Size = new System.Drawing.Size(273, 28);
			this.comboBoxFormula.TabIndex = 1;
			this.comboBoxFormula.SelectedIndexChanged += new System.EventHandler(this.comboBoxFormula_SelectedIndexChanged);
			// 
			// pictureBoxFormula
			// 
			this.pictureBoxFormula.Location = new System.Drawing.Point(17, 49);
			this.pictureBoxFormula.Name = "pictureBoxFormula";
			this.pictureBoxFormula.Size = new System.Drawing.Size(502, 63);
			this.pictureBoxFormula.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBoxFormula.TabIndex = 10;
			this.pictureBoxFormula.TabStop = false;
			// 
			// pictureBoxBern4
			// 
			this.pictureBoxBern4.Image = global::Lab3.Properties.Resources.FormulaBern4;
			this.pictureBoxBern4.Location = new System.Drawing.Point(336, 142);
			this.pictureBoxBern4.Name = "pictureBoxBern4";
			this.pictureBoxBern4.Size = new System.Drawing.Size(183, 50);
			this.pictureBoxBern4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBoxBern4.TabIndex = 9;
			this.pictureBoxBern4.TabStop = false;
			this.pictureBoxBern4.Click += new System.EventHandler(this.pictureBoxBern4_Click);
			// 
			// pictureBoxBern3
			// 
			this.pictureBoxBern3.Image = global::Lab3.Properties.Resources.FormulaBern3;
			this.pictureBoxBern3.Location = new System.Drawing.Point(229, 142);
			this.pictureBoxBern3.Name = "pictureBoxBern3";
			this.pictureBoxBern3.Size = new System.Drawing.Size(100, 50);
			this.pictureBoxBern3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBoxBern3.TabIndex = 8;
			this.pictureBoxBern3.TabStop = false;
			this.pictureBoxBern3.Click += new System.EventHandler(this.pictureBoxBern3_Click);
			// 
			// pictureBoxBern2
			// 
			this.pictureBoxBern2.Image = global::Lab3.Properties.Resources.FormulaBern2;
			this.pictureBoxBern2.Location = new System.Drawing.Point(123, 142);
			this.pictureBoxBern2.Name = "pictureBoxBern2";
			this.pictureBoxBern2.Size = new System.Drawing.Size(100, 50);
			this.pictureBoxBern2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBoxBern2.TabIndex = 7;
			this.pictureBoxBern2.TabStop = false;
			this.pictureBoxBern2.Click += new System.EventHandler(this.pictureBoxBern2_Click);
			// 
			// pictureBoxBern1
			// 
			this.pictureBoxBern1.Image = global::Lab3.Properties.Resources.FormulaBern1;
			this.pictureBoxBern1.Location = new System.Drawing.Point(17, 142);
			this.pictureBoxBern1.Name = "pictureBoxBern1";
			this.pictureBoxBern1.Size = new System.Drawing.Size(100, 50);
			this.pictureBoxBern1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBoxBern1.TabIndex = 6;
			this.pictureBoxBern1.TabStop = false;
			this.pictureBoxBern1.Click += new System.EventHandler(this.pictureBoxBern1_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label2.Location = new System.Drawing.Point(20, 211);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(18, 20);
			this.label2.TabIndex = 11;
			this.label2.Text = "p";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label3.Location = new System.Drawing.Point(18, 243);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(18, 20);
			this.label3.TabIndex = 12;
			this.label3.Text = "n";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label4.Location = new System.Drawing.Point(17, 307);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(22, 20);
			this.label4.TabIndex = 13;
			this.label4.Text = "m";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label5.Location = new System.Drawing.Point(17, 340);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(27, 20);
			this.label5.TabIndex = 14;
			this.label5.Text = "m₁";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label6.Location = new System.Drawing.Point(17, 371);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(27, 20);
			this.label6.TabIndex = 15;
			this.label6.Text = "m₂";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label7.Location = new System.Drawing.Point(17, 276);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(17, 20);
			this.label7.TabIndex = 16;
			this.label7.Text = "k";
			// 
			// textBoxP
			// 
			this.textBoxP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxP.Location = new System.Drawing.Point(60, 208);
			this.textBoxP.Name = "textBoxP";
			this.textBoxP.Size = new System.Drawing.Size(100, 26);
			this.textBoxP.TabIndex = 17;
			this.textBoxP.TextChanged += new System.EventHandler(this.textBoxP_TextChanged);
			this.textBoxP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxP_KeyPress);
			// 
			// textBoxN
			// 
			this.textBoxN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxN.Location = new System.Drawing.Point(60, 240);
			this.textBoxN.Name = "textBoxN";
			this.textBoxN.Size = new System.Drawing.Size(100, 26);
			this.textBoxN.TabIndex = 18;
			this.textBoxN.TextChanged += new System.EventHandler(this.textBoxN_TextChanged);
			this.textBoxN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxN_KeyPress);
			// 
			// textBoxK
			// 
			this.textBoxK.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxK.Location = new System.Drawing.Point(60, 273);
			this.textBoxK.Name = "textBoxK";
			this.textBoxK.Size = new System.Drawing.Size(100, 26);
			this.textBoxK.TabIndex = 19;
			this.textBoxK.TextChanged += new System.EventHandler(this.textBoxK_TextChanged);
			this.textBoxK.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxK_KeyPress);
			// 
			// textBoxM
			// 
			this.textBoxM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxM.Location = new System.Drawing.Point(60, 304);
			this.textBoxM.Name = "textBoxM";
			this.textBoxM.Size = new System.Drawing.Size(100, 26);
			this.textBoxM.TabIndex = 20;
			this.textBoxM.TextChanged += new System.EventHandler(this.textBoxM_TextChanged);
			this.textBoxM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxM_KeyPress);
			// 
			// textBoxM1
			// 
			this.textBoxM1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxM1.Location = new System.Drawing.Point(60, 337);
			this.textBoxM1.Name = "textBoxM1";
			this.textBoxM1.Size = new System.Drawing.Size(100, 26);
			this.textBoxM1.TabIndex = 21;
			this.textBoxM1.TextChanged += new System.EventHandler(this.textBoxM1_TextChanged);
			this.textBoxM1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxM1_KeyPress);
			// 
			// textBoxM2
			// 
			this.textBoxM2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxM2.Location = new System.Drawing.Point(60, 368);
			this.textBoxM2.Name = "textBoxM2";
			this.textBoxM2.Size = new System.Drawing.Size(100, 26);
			this.textBoxM2.TabIndex = 22;
			this.textBoxM2.TextChanged += new System.EventHandler(this.textBoxM2_TextChanged);
			this.textBoxM2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxM2_KeyPress);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label8.Location = new System.Drawing.Point(219, 263);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(102, 20);
			this.label8.TabIndex = 23;
			this.label8.Text = "m₁, m₂, ..., mk";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label9.Location = new System.Drawing.Point(219, 295);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(90, 20);
			this.label9.TabIndex = 24;
			this.label9.Text = "p₁, p₂, ..., pk";
			// 
			// textBoxMk
			// 
			this.textBoxMk.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxMk.Location = new System.Drawing.Point(337, 260);
			this.textBoxMk.Name = "textBoxMk";
			this.textBoxMk.Size = new System.Drawing.Size(182, 26);
			this.textBoxMk.TabIndex = 25;
			this.textBoxMk.TextChanged += new System.EventHandler(this.textBoxMk_TextChanged);
			this.textBoxMk.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxMk_KeyPress);
			// 
			// textBoxPk
			// 
			this.textBoxPk.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxPk.Location = new System.Drawing.Point(337, 292);
			this.textBoxPk.Name = "textBoxPk";
			this.textBoxPk.Size = new System.Drawing.Size(182, 26);
			this.textBoxPk.TabIndex = 26;
			this.textBoxPk.TextChanged += new System.EventHandler(this.textBoxPk_TextChanged);
			this.textBoxPk.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPk_KeyPress);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label10.Location = new System.Drawing.Point(18, 413);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(181, 20);
			this.label10.TabIndex = 27;
			this.label10.Text = "Вероятность события:";
			// 
			// labelResult
			// 
			this.labelResult.AutoSize = true;
			this.labelResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelResult.Location = new System.Drawing.Point(17, 445);
			this.labelResult.Name = "labelResult";
			this.labelResult.Size = new System.Drawing.Size(18, 20);
			this.labelResult.TabIndex = 28;
			this.labelResult.Text = "0";
			// 
			// FormLab3
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(545, 489);
			this.Controls.Add(this.labelResult);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.textBoxPk);
			this.Controls.Add(this.textBoxMk);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.textBoxM2);
			this.Controls.Add(this.textBoxM1);
			this.Controls.Add(this.textBoxM);
			this.Controls.Add(this.textBoxK);
			this.Controls.Add(this.textBoxN);
			this.Controls.Add(this.textBoxP);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.pictureBoxFormula);
			this.Controls.Add(this.pictureBoxBern4);
			this.Controls.Add(this.pictureBoxBern3);
			this.Controls.Add(this.pictureBoxBern2);
			this.Controls.Add(this.pictureBoxBern1);
			this.Controls.Add(this.comboBoxFormula);
			this.Controls.Add(this.label1);
			this.Name = "FormLab3";
			this.Text = "Лабораторная №3";
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxFormula)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxBern4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxBern3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxBern2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxBern1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox comboBoxFormula;
		private System.Windows.Forms.PictureBox pictureBoxBern1;
		private System.Windows.Forms.PictureBox pictureBoxBern2;
		private System.Windows.Forms.PictureBox pictureBoxBern3;
		private System.Windows.Forms.PictureBox pictureBoxBern4;
		private System.Windows.Forms.PictureBox pictureBoxFormula;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBoxP;
		private System.Windows.Forms.TextBox textBoxN;
		private System.Windows.Forms.TextBox textBoxK;
		private System.Windows.Forms.TextBox textBoxM;
		private System.Windows.Forms.TextBox textBoxM1;
		private System.Windows.Forms.TextBox textBoxM2;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox textBoxMk;
		private System.Windows.Forms.TextBox textBoxPk;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label labelResult;
	}
}

