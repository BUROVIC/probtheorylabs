﻿namespace Lab2
{
	partial class FormLab2
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.comboBoxTask = new System.Windows.Forms.ComboBox();
			this.labelResultIs = new System.Windows.Forms.Label();
			this.labelResult = new System.Windows.Forms.Label();
			this.textBoxProbs = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.pictureBoxTask = new System.Windows.Forms.PictureBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label31 = new System.Windows.Forms.Label();
			this.label41 = new System.Windows.Forms.Label();
			this.textBoxSpecial4 = new System.Windows.Forms.TextBox();
			this.pictureBox4 = new System.Windows.Forms.PictureBox();
			this.pictureBox41 = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxTask)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).BeginInit();
			this.SuspendLayout();
			// 
			// comboBoxTask
			// 
			this.comboBoxTask.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxTask.DropDownWidth = 390;
			this.comboBoxTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.comboBoxTask.FormattingEnabled = true;
			this.comboBoxTask.Items.AddRange(new object[] {
            "Задание 1. Нахождение вероятности безотказной работы.",
            "Задание 2. Схема придуманная.",
            "Задание 3. Выбор подходящих событий. Задача 1.",
            "Задание 3-1. Выбор подходящих событий. Задача 2.",
            "Задание 4. Формулы полной вероятности и Байеса. Задача 1.",
            "Задание 4-1. Формулы полной вероятности и Байеса. Задача 2."});
			this.comboBoxTask.Location = new System.Drawing.Point(16, 12);
			this.comboBoxTask.Name = "comboBoxTask";
			this.comboBoxTask.Size = new System.Drawing.Size(266, 24);
			this.comboBoxTask.TabIndex = 0;
			this.comboBoxTask.SelectedIndexChanged += new System.EventHandler(this.comboBoxTask_SelectedIndexChanged);
			// 
			// labelResultIs
			// 
			this.labelResultIs.AutoSize = true;
			this.labelResultIs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelResultIs.Location = new System.Drawing.Point(12, 327);
			this.labelResultIs.Name = "labelResultIs";
			this.labelResultIs.Size = new System.Drawing.Size(93, 20);
			this.labelResultIs.TabIndex = 22;
			this.labelResultIs.Text = "Результат:";
			// 
			// labelResult
			// 
			this.labelResult.AutoSize = true;
			this.labelResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelResult.Location = new System.Drawing.Point(12, 347);
			this.labelResult.Name = "labelResult";
			this.labelResult.Size = new System.Drawing.Size(18, 20);
			this.labelResult.TabIndex = 21;
			this.labelResult.Text = "0";
			// 
			// textBoxProbs
			// 
			this.textBoxProbs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxProbs.Location = new System.Drawing.Point(16, 241);
			this.textBoxProbs.Name = "textBoxProbs";
			this.textBoxProbs.Size = new System.Drawing.Size(338, 26);
			this.textBoxProbs.TabIndex = 23;
			this.textBoxProbs.TextChanged += new System.EventHandler(this.textBoxProbs_TextChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(12, 218);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(455, 20);
			this.label1.TabIndex = 24;
			this.label1.Text = "Вероятности отказа i-ых элементов через точку с запятой";
			// 
			// pictureBoxTask
			// 
			this.pictureBoxTask.Location = new System.Drawing.Point(16, 43);
			this.pictureBoxTask.Name = "pictureBoxTask";
			this.pictureBoxTask.Size = new System.Drawing.Size(536, 160);
			this.pictureBoxTask.TabIndex = 25;
			this.pictureBoxTask.TabStop = false;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.label4.Location = new System.Drawing.Point(12, 218);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(290, 20);
			this.label4.TabIndex = 26;
			this.label4.Text = "События, образующие полную группу";
			this.label4.Visible = false;
			// 
			// label31
			// 
			this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.label31.Location = new System.Drawing.Point(12, 218);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(516, 23);
			this.label31.TabIndex = 27;
			this.label31.Text = "Вероятности попадания в цель i-го стрелка через точку с запятой";
			this.label31.Visible = false;
			// 
			// label41
			// 
			this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.label41.Location = new System.Drawing.Point(12, 270);
			this.label41.Name = "label41";
			this.label41.Size = new System.Drawing.Size(375, 23);
			this.label41.TabIndex = 29;
			this.label41.Text = "Вероятности гипотиз и условных вероятностей";
			this.label41.Visible = false;
			// 
			// textBoxSpecial4
			// 
			this.textBoxSpecial4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.textBoxSpecial4.Location = new System.Drawing.Point(16, 297);
			this.textBoxSpecial4.Name = "textBoxSpecial4";
			this.textBoxSpecial4.Size = new System.Drawing.Size(338, 26);
			this.textBoxSpecial4.TabIndex = 30;
			// 
			// pictureBox4
			// 
			this.pictureBox4.Location = new System.Drawing.Point(386, 244);
			this.pictureBox4.Name = "pictureBox4";
			this.pictureBox4.Size = new System.Drawing.Size(412, 47);
			this.pictureBox4.TabIndex = 31;
			this.pictureBox4.TabStop = false;
			this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
			// 
			// pictureBox41
			// 
			this.pictureBox41.Location = new System.Drawing.Point(386, 297);
			this.pictureBox41.Name = "pictureBox41";
			this.pictureBox41.Size = new System.Drawing.Size(412, 50);
			this.pictureBox41.TabIndex = 32;
			this.pictureBox41.TabStop = false;
			this.pictureBox41.Click += new System.EventHandler(this.pictureBox41_Click);
			// 
			// FormLab2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(838, 488);
			this.Controls.Add(this.pictureBox41);
			this.Controls.Add(this.pictureBox4);
			this.Controls.Add(this.textBoxSpecial4);
			this.Controls.Add(this.label41);
			this.Controls.Add(this.label31);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.pictureBoxTask);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBoxProbs);
			this.Controls.Add(this.labelResultIs);
			this.Controls.Add(this.labelResult);
			this.Controls.Add(this.comboBoxTask);
			this.Name = "FormLab2";
			this.Text = "Лабораторная №2";
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxTask)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox comboBoxTask;
		private System.Windows.Forms.Label labelResultIs;
		private System.Windows.Forms.Label labelResult;
		private System.Windows.Forms.TextBox textBoxProbs;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.PictureBox pictureBoxTask;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBoxSpecial4;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox41;
	}
}

