﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Windows.Forms;

namespace Lab2
{
	public partial class FormLab2 : Form
	{
		public FormLab2()
		{
			InitializeComponent();
			pictureBox4.BorderStyle = BorderStyle.Fixed3D;
			comboBoxTask.SelectedIndex = 0;
			pictureBoxTask.SizeMode = PictureBoxSizeMode.StretchImage;
		}

		private void comboBoxTask_SelectedIndexChanged(object sender, EventArgs e)
		{
           
			ResourceManager rm = new ResourceManager(typeof(Lab2.Properties.Resources));
			pictureBox4.Visible = false;
			pictureBox41.Visible = false;
			switch ((sender as ComboBox).SelectedIndex)
			{
				case 0:
                    SetLable(0);
					labelResult.Text = calculateScheme(0).ToString();
					pictureBoxTask.Image = Properties.Resources.Task1;
					break;
				case 1:
                    SetLable(0);
					labelResult.Text = calculateScheme(1).ToString();
					pictureBoxTask.Image = Properties.Resources.Task2;
					break;
				case 2:
                    SetLable(0);
					pictureBoxTask.Image = Properties.Resources.Task3;
                    var probs = getProbs();

                    if (probs != null && probs.Count == 4)
                    {
                        labelResult.Text = "Только один станок потребует наладки: " + ((probs[0] * (1 - probs[1]) * (1 - probs[2]) * (1 - probs[3])) + (probs[1] * (1 - probs[0]) * (1 - probs[2]) * (1 - probs[3])) + 
                                                                                       (probs[2] * (1 - probs[0]) * (1 - probs[1]) * (1 - probs[3])) + (probs[3] * (1 - probs[0]) * (1 - probs[1]) * (1 - probs[2]))).ToString()

                            + "\nТолько 3-ий станок потребует наладки: " + ((1 - probs[0]) * (1 - probs[1]) * probs[2] * (1 - probs[3])).ToString()

                          
                            + "\nТолько два станка потребуют наладки: " + (probs[0] * probs[1] * (1 - probs[2]) * (1 - probs[3]) + probs[0] * probs[2] * (1 - probs[1]) * (1 - probs[3]) + probs[0] * probs[3] * (1 - probs[1]) * (1 - probs[2]) + 
                                                                           probs[1] * probs[2] * (1 - probs[0]) * (1 - probs[3]) + probs[1] * probs[3] * (1 - probs[2]) * (1 - probs[0]) + probs[2] * probs[3] * (1 - probs[0]) * (1 - probs[1])).ToString()

                            + "\nХотя бы один станок потребует наладки: " + (1 - (probs[0] * probs[1] * probs[2] * probs[3])).ToString();
                    }
                  
                //probs[probs.Count - 1].ToString();
					break;
				case 3:
                    SetLable(1);
                    pictureBoxTask.Image = Properties.Resources.Task4;

                    var prob = getProbs();

                    if (prob != null && prob.Count == 4)
                    {
                        labelResult.Text = "Только один стрелок попадет в цель: " + (((1 - prob[0]) *  prob[1] *  prob[2] * prob[3]) + ((1 - prob[1]) *  prob[0] *  prob[2] * prob[3]) + 
                                                                                       ((1-prob[2]) * prob[0] * prob[1] * prob[3]) + ((1 - prob[3]) *  prob[0] *  prob[1] *  prob[2])).ToString()

                            + "\nТолько 3-ий стрелок попадет цель: " + (prob[0] * prob[1] * prob[3] * (1 - prob[2])).ToString()

                          
                            + "\nТолько два стрелка попадут в цель: " + (prob[0] * prob[1] * (1 - prob[2]) * (1 - prob[3]) + prob[0] * prob[2] * (1 - prob[1]) * (1 - prob[3]) + prob[0] * prob[3] * (1 - prob[1]) * (1 - prob[2]) + 
                                                                           prob[1] * prob[2] * (1 - prob[0]) * (1 - prob[3]) + prob[1] * prob[3] * (1 - prob[2]) * (1 - prob[0]) + prob[2] * prob[3] * (1 - prob[0]) * (1 - prob[1])).ToString()

                            + "\nХотя бы один стрелок попадет в цель: " + (1 - (prob[0] * prob[1] * prob[2] * prob[3])).ToString();
                    }
					
					break;
                case 4:
                    SetLable(2);
                    pictureBoxTask.Image = Properties.Resources.Task5;
					pictureBox4.Visible = true;
					pictureBox41.Visible = true;
					pictureBox4.Image = Properties.Resources.Formula1;
                    pictureBox41.Image = Properties.Resources.Formula2;
                   var pro = getProbs1();

                   if (pro != null)
                   {
						if (pictureBox4.BorderStyle == BorderStyle.Fixed3D && pro.Count == 6)
						{
							labelResult.Text = (pro[0] * pro[3] + pro[1] * pro[4] + pro[2] * pro[5]).ToString();
						}
						else if(pro.Count == 3)
						{
							labelResult.Text = ((pro[0] * pro[1]) / (pro[2])).ToString();
						}
                   }
                 break;
                case 5:
                    SetLable(2);
                    pictureBoxTask.Image = Properties.Resources.Task6;
					pictureBox4.Visible = true;
					pictureBox41.Visible = true;
					pictureBox4.Image = Properties.Resources.Formula1;
                    pictureBox41.Image = Properties.Resources.Formula2;

                     var pro5 = getProbs1();

                   if (pro5 != null)
                   {
						if (pictureBox4.BorderStyle == BorderStyle.Fixed3D && pro5.Count == 6)
						{
							labelResult.Text = (pro5[0] * pro5[3] + pro5[1] * pro5[4] + pro5[2] * pro5[5]).ToString();
						}
						else if (pro5.Count == 3)
						{
							labelResult.Text = ((pro5[0] * pro5[1]) / (pro5[2])).ToString();
						}
                   }
                 break;


                   break;
				default:
					break;
			}
		}

        private void SetLable(int i) {
            if (i == 0)
            {
                label1.Visible = true;
                label31.Visible = false;
                label4.Visible = false;
                label41.Visible = false;
            }
            if (i == 1)
            {
                label1.Visible = false;
                label31.Visible = true;
                label4.Visible = false;
                label41.Visible = false;
            }
            if (i == 2)
            {
                label1.Visible = false;
                label31.Visible = false;
                label4.Visible = true;
                label41.Visible = true;
            }
        }
		//TODO: Обобщить функцию для ЛЮБОЙ схемы
		/// <summary>
		/// Вероятность отказа схемы
		/// </summary>
		/// <param name="type">Тип схемы</param>
		/// <returns>Вероятность отказа схемы</returns>
		private float calculateScheme(int type)
		{
			var probs = getProbs();
            float result = 0;
            if (probs != null && probs.Count == 6)
            {
                if (type == 0)
                {
                    result = probs[1]*probs[4]+probs[1]*probs[5]+probs[0]+probs[2]*probs[3]*probs[4]+probs[2]*probs[3]*probs[5]; //probs[1] * probs[4] + probs[1] * probs[5] + probs[2] * probs[3] * probs[4] + probs[2] * probs[3] * probs[5] + probs[0] * probs[1] * probs[2] * probs[3] * probs[4] * probs[5] + probs[1] * probs[2] * probs[3] * probs[4] * probs[5] + probs[0];
                }
                else
                {
                    result = probs[2] * (probs[4] * probs[5] * (1 - probs[3]) + probs[3]) * (1 - probs[1] * probs[0]) + probs[0] * probs[1];
                }
            }
			return result;
		}

		/// <summary>
		/// Вероятности отказа i-ых элементов
		/// </summary>
		/// <returns>Массив вероятностей</returns>
		private List<float> getProbs()
		{
			//TODO: нормальная проверка правильности заданной строки
			try
			{
				return new List<float>(textBoxProbs.Text.Replace(" ", string.Empty).Split(';').Select(float.Parse).ToList());
			}
			catch (Exception)
			{
				return null;
			}
			
		}
        /// <summary>
        /// Вероятности гипотез условных вероятностей
        /// </summary>
        /// <returns>Массив вероятностей</returns>
        private List<float> getProbs1()
        {
            //TODO: нормальная проверка правильности заданной строки
            try
            {
                return new List<float>(textBoxSpecial4.Text.Replace(" ", string.Empty).Split(';').Select(float.Parse).ToList());
            }
            catch (Exception)
            {
                return null;
            }

        }

		private void textBoxProbs_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) 
				&& e.KeyChar != ',' && e.KeyChar != ' ' && e.KeyChar != ';')
			{
				e.Handled = true;
			}
		}

        private void textBoxProbs_TextChanged(object sender, EventArgs e)
        {
            comboBoxTask_SelectedIndexChanged(comboBoxTask, e);
        }

		private void pictureBox4_Click(object sender, EventArgs e)
		{
			pictureBox4.BorderStyle = BorderStyle.Fixed3D;
			pictureBox41.BorderStyle = BorderStyle.None;
			comboBoxTask_SelectedIndexChanged(comboBoxTask, e);
		}

		private void pictureBox41_Click(object sender, EventArgs e)
		{
			pictureBox41.BorderStyle = BorderStyle.Fixed3D;
			pictureBox4.BorderStyle = BorderStyle.None;
			comboBoxTask_SelectedIndexChanged(comboBoxTask, e);
		}
	}
}
