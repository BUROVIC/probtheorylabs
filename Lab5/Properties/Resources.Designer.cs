﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Lab5.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Lab5.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 40.1
        ///40.5
        ///40.3
        ///40.7
        ///40.6
        ///40.2
        ///40.6
        ///40.4
        ///40.8
        ///40.7
        ///40.3
        ///40.1
        ///40.7
        ///40.3
        ///40.6
        ///40.5
        ///40.8
        ///40.6
        ///40.6
        ///40.4
        ///40.5
        ///40.6
        ///40.1
        ///40.6
        ///40.2
        ///40.5
        ///40.3
        ///40.6
        ///40.4
        ///40.6
        ///40.5
        ///40.3
        ///40.7
        ///40.5
        ///40.9
        ///40.8
        ///40.5
        ///40.6
        ///40.7
        ///40.6
        ///40.6
        ///40.2
        ///40.6
        ///40.6
        ///40.5
        ///40.3
        ///40.8
        ///40.4
        ///40.5
        ///40.4
        ///40.4
        ///40.7
        ///40.6
        ///40.2
        ///40.6
        ///40.9
        ///40.3
        ///40.7
        ///40.6
        ///40.6
        ///40.7
        ///40.6
        ///40.5
        ///40.8
        ///40.5
        ///40.6
        ///40.8
        ///41.0
        ///40.4
        ///41.0
        ///40.6
        ///40.4
        ///40.8
        ///40.2
        ///40.9
        ///40.4
        ///40.5
        ///40.3
        ///40.5
        ///40.7
        ///40.7
        ///40.5
        ///40.5
        ///40.8
        ///40.3
        ///40 [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Data {
            get {
                return ResourceManager.GetString("Data", resourceCulture);
            }
        }
    }
}
