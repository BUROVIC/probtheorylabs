﻿namespace Lab5
{
	partial class FormLab5
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLab5));
			this.tableLayoutPanelPrimary = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanelVariat = new System.Windows.Forms.TableLayoutPanel();
			this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.labelFx = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.pictureBox3 = new System.Windows.Forms.PictureBox();
			this.pictureBox4 = new System.Windows.Forms.PictureBox();
			this.labelXv = new System.Windows.Forms.Label();
			this.labelDv = new System.Windows.Forms.Label();
			this.labelDfixed = new System.Windows.Forms.Label();
			this.labelDfixedSqrt = new System.Windows.Forms.Label();
			this.tableLayoutPanelInterval = new System.Windows.Forms.TableLayoutPanel();
			this.checkBoxBasedOnInterval = new System.Windows.Forms.CheckBox();
			this.checkBoxCustomIntervals = new System.Windows.Forms.CheckBox();
			this.textBoxIntervalsCount = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
			this.SuspendLayout();
			// 
			// tableLayoutPanelPrimary
			// 
			this.tableLayoutPanelPrimary.AutoScroll = true;
			this.tableLayoutPanelPrimary.AutoScrollMinSize = new System.Drawing.Size(99, 1);
			this.tableLayoutPanelPrimary.ColumnCount = 1;
			this.tableLayoutPanelPrimary.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanelPrimary.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
			this.tableLayoutPanelPrimary.Location = new System.Drawing.Point(12, 29);
			this.tableLayoutPanelPrimary.MaximumSize = new System.Drawing.Size(600, 500);
			this.tableLayoutPanelPrimary.Name = "tableLayoutPanelPrimary";
			this.tableLayoutPanelPrimary.RowCount = 1;
			this.tableLayoutPanelPrimary.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanelPrimary.Size = new System.Drawing.Size(600, 35);
			this.tableLayoutPanelPrimary.TabIndex = 1;
			// 
			// tableLayoutPanelVariat
			// 
			this.tableLayoutPanelVariat.AutoScroll = true;
			this.tableLayoutPanelVariat.AutoScrollMinSize = new System.Drawing.Size(99, 1);
			this.tableLayoutPanelVariat.ColumnCount = 1;
			this.tableLayoutPanelVariat.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanelVariat.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
			this.tableLayoutPanelVariat.Location = new System.Drawing.Point(12, 113);
			this.tableLayoutPanelVariat.MaximumSize = new System.Drawing.Size(600, 500);
			this.tableLayoutPanelVariat.Name = "tableLayoutPanelVariat";
			this.tableLayoutPanelVariat.RowCount = 3;
			this.tableLayoutPanelVariat.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanelVariat.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
			this.tableLayoutPanelVariat.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
			this.tableLayoutPanelVariat.Size = new System.Drawing.Size(600, 85);
			this.tableLayoutPanelVariat.TabIndex = 2;
			// 
			// chart1
			// 
			chartArea1.Name = "ChartArea1";
			this.chart1.ChartAreas.Add(chartArea1);
			legend1.Name = "Legend1";
			this.chart1.Legends.Add(legend1);
			this.chart1.Location = new System.Drawing.Point(8, 384);
			this.chart1.Name = "chart1";
			series1.ChartArea = "ChartArea1";
			series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series1.Legend = "Legend1";
			series1.Name = "W [i]";
			this.chart1.Series.Add(series1);
			this.chart1.Size = new System.Drawing.Size(400, 300);
			this.chart1.TabIndex = 3;
			this.chart1.Text = "chart1";
			// 
			// chart2
			// 
			chartArea2.Name = "ChartArea1";
			this.chart2.ChartAreas.Add(chartArea2);
			legend2.Name = "Legend1";
			this.chart2.Legends.Add(legend2);
			this.chart2.Location = new System.Drawing.Point(438, 384);
			this.chart2.Name = "chart2";
			series2.ChartArea = "ChartArea1";
			series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series2.Legend = "Legend1";
			series2.Name = "W [i]/n";
			this.chart2.Series.Add(series2);
			this.chart2.Size = new System.Drawing.Size(400, 300);
			this.chart2.TabIndex = 4;
			this.chart2.Text = "chart2";
			// 
			// chart3
			// 
			chartArea3.Name = "ChartArea1";
			this.chart3.ChartAreas.Add(chartArea3);
			legend3.Name = "Legend1";
			this.chart3.Legends.Add(legend3);
			this.chart3.Location = new System.Drawing.Point(438, 690);
			this.chart3.Name = "chart3";
			series3.ChartArea = "ChartArea1";
			series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
			series3.Legend = "Legend1";
			series3.Name = "F(x)";
			this.chart3.Series.Add(series3);
			this.chart3.Size = new System.Drawing.Size(400, 300);
			this.chart3.TabIndex = 5;
			this.chart3.Text = "chart3";
			this.chart3.PrePaint += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ChartPaintEventArgs>(this.chart3_PrePaint);
			// 
			// labelFx
			// 
			this.labelFx.AutoSize = true;
			this.labelFx.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelFx.Location = new System.Drawing.Point(8, 705);
			this.labelFx.Name = "labelFx";
			this.labelFx.Size = new System.Drawing.Size(40, 16);
			this.labelFx.TabIndex = 6;
			this.labelFx.Text = "F(x) =";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(638, 29);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(345, 65);
			this.pictureBox1.TabIndex = 7;
			this.pictureBox1.TabStop = false;
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(638, 100);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(258, 65);
			this.pictureBox2.TabIndex = 8;
			this.pictureBox2.TabStop = false;
			// 
			// pictureBox3
			// 
			this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
			this.pictureBox3.Location = new System.Drawing.Point(638, 171);
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.Size = new System.Drawing.Size(143, 65);
			this.pictureBox3.TabIndex = 9;
			this.pictureBox3.TabStop = false;
			// 
			// pictureBox4
			// 
			this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
			this.pictureBox4.Location = new System.Drawing.Point(883, 181);
			this.pictureBox4.Name = "pictureBox4";
			this.pictureBox4.Size = new System.Drawing.Size(100, 55);
			this.pictureBox4.TabIndex = 10;
			this.pictureBox4.TabStop = false;
			// 
			// labelXv
			// 
			this.labelXv.AutoSize = true;
			this.labelXv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelXv.Location = new System.Drawing.Point(989, 44);
			this.labelXv.Name = "labelXv";
			this.labelXv.Size = new System.Drawing.Size(51, 20);
			this.labelXv.TabIndex = 11;
			this.labelXv.Text = "label1";
			// 
			// labelDv
			// 
			this.labelDv.AutoSize = true;
			this.labelDv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelDv.Location = new System.Drawing.Point(902, 122);
			this.labelDv.Name = "labelDv";
			this.labelDv.Size = new System.Drawing.Size(51, 20);
			this.labelDv.TabIndex = 12;
			this.labelDv.Text = "label1";
			// 
			// labelDfixed
			// 
			this.labelDfixed.AutoSize = true;
			this.labelDfixed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelDfixed.Location = new System.Drawing.Point(787, 194);
			this.labelDfixed.Name = "labelDfixed";
			this.labelDfixed.Size = new System.Drawing.Size(51, 20);
			this.labelDfixed.TabIndex = 13;
			this.labelDfixed.Text = "label1";
			// 
			// labelDfixedSqrt
			// 
			this.labelDfixedSqrt.AutoSize = true;
			this.labelDfixedSqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelDfixedSqrt.Location = new System.Drawing.Point(989, 194);
			this.labelDfixedSqrt.Name = "labelDfixedSqrt";
			this.labelDfixedSqrt.Size = new System.Drawing.Size(51, 20);
			this.labelDfixedSqrt.TabIndex = 14;
			this.labelDfixedSqrt.Text = "label1";
			// 
			// tableLayoutPanelInterval
			// 
			this.tableLayoutPanelInterval.AutoScroll = true;
			this.tableLayoutPanelInterval.AutoScrollMinSize = new System.Drawing.Size(99, 1);
			this.tableLayoutPanelInterval.ColumnCount = 1;
			this.tableLayoutPanelInterval.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanelInterval.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
			this.tableLayoutPanelInterval.Location = new System.Drawing.Point(12, 227);
			this.tableLayoutPanelInterval.MaximumSize = new System.Drawing.Size(600, 500);
			this.tableLayoutPanelInterval.Name = "tableLayoutPanelInterval";
			this.tableLayoutPanelInterval.RowCount = 4;
			this.tableLayoutPanelInterval.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanelInterval.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
			this.tableLayoutPanelInterval.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
			this.tableLayoutPanelInterval.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
			this.tableLayoutPanelInterval.Size = new System.Drawing.Size(600, 151);
			this.tableLayoutPanelInterval.TabIndex = 15;
			// 
			// checkBoxBasedOnInterval
			// 
			this.checkBoxBasedOnInterval.AutoSize = true;
			this.checkBoxBasedOnInterval.Location = new System.Drawing.Point(618, 285);
			this.checkBoxBasedOnInterval.Name = "checkBoxBasedOnInterval";
			this.checkBoxBasedOnInterval.Size = new System.Drawing.Size(185, 17);
			this.checkBoxBasedOnInterval.TabIndex = 16;
			this.checkBoxBasedOnInterval.Text = "На основе интервального ряда";
			this.checkBoxBasedOnInterval.UseVisualStyleBackColor = true;
			this.checkBoxBasedOnInterval.CheckedChanged += new System.EventHandler(this.checkBoxBasedOnInterval_CheckedChanged);
			// 
			// checkBoxCustomIntervals
			// 
			this.checkBoxCustomIntervals.AutoSize = true;
			this.checkBoxCustomIntervals.Location = new System.Drawing.Point(618, 262);
			this.checkBoxCustomIntervals.Name = "checkBoxCustomIntervals";
			this.checkBoxCustomIntervals.Size = new System.Drawing.Size(112, 17);
			this.checkBoxCustomIntervals.TabIndex = 17;
			this.checkBoxCustomIntervals.Text = "Ввести интервал";
			this.checkBoxCustomIntervals.UseVisualStyleBackColor = true;
			this.checkBoxCustomIntervals.CheckedChanged += new System.EventHandler(this.checkBoxCustomIntervals_CheckedChanged);
			// 
			// textBoxIntervalsCount
			// 
			this.textBoxIntervalsCount.Location = new System.Drawing.Point(618, 309);
			this.textBoxIntervalsCount.Name = "textBoxIntervalsCount";
			this.textBoxIntervalsCount.Size = new System.Drawing.Size(100, 20);
			this.textBoxIntervalsCount.TabIndex = 18;
			this.textBoxIntervalsCount.TextChanged += new System.EventHandler(this.textBoxIntervalsCount_TextChanged);
			this.textBoxIntervalsCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
			// 
			// FormLab5
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.ClientSize = new System.Drawing.Size(1170, 873);
			this.Controls.Add(this.textBoxIntervalsCount);
			this.Controls.Add(this.checkBoxCustomIntervals);
			this.Controls.Add(this.checkBoxBasedOnInterval);
			this.Controls.Add(this.tableLayoutPanelInterval);
			this.Controls.Add(this.labelDfixedSqrt);
			this.Controls.Add(this.labelDfixed);
			this.Controls.Add(this.labelDv);
			this.Controls.Add(this.labelXv);
			this.Controls.Add(this.pictureBox4);
			this.Controls.Add(this.pictureBox3);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.labelFx);
			this.Controls.Add(this.chart3);
			this.Controls.Add(this.chart2);
			this.Controls.Add(this.chart1);
			this.Controls.Add(this.tableLayoutPanelVariat);
			this.Controls.Add(this.tableLayoutPanelPrimary);
			this.Name = "FormLab5";
			this.Text = "Лабораторная №5";
			((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanelPrimary;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanelVariat;
		private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
		private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
		private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
		private System.Windows.Forms.Label labelFx;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.PictureBox pictureBox3;
		private System.Windows.Forms.PictureBox pictureBox4;
		private System.Windows.Forms.Label labelXv;
		private System.Windows.Forms.Label labelDv;
		private System.Windows.Forms.Label labelDfixed;
		private System.Windows.Forms.Label labelDfixedSqrt;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanelInterval;
		private System.Windows.Forms.CheckBox checkBoxBasedOnInterval;
		private System.Windows.Forms.CheckBox checkBoxCustomIntervals;
		private System.Windows.Forms.TextBox textBoxIntervalsCount;
	}
}

