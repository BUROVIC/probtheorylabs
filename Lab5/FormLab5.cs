﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab5
{
	public partial class FormLab5 : Form
	{
		//Исходные данные
		List<float> Data { get; set; }
		Dictionary<float, ValueCharacteristics> ValuesCharacteristics { get; set; }
		List<float> Values
		{
			get
			{
				return ValuesCharacteristics.Select(x => x.Key).ToList();
			}
		}

		public FormLab5()
		{
			InitializeComponent();

            var dataStr = Properties.Resources.Data.ToString();
            try
            {
                Data = dataStr.Split('\n').Select(float.Parse).ToList();
            }
            catch (Exception)
            {
                Data = dataStr.Replace('.', ',').Split('\n').Select(float.Parse).ToList();
            }

			#region Настройка лайаутов
			tableLayoutPanelPrimary.ColumnCount = 0;
			tableLayoutPanelPrimary.RowCount = 1;
			tableLayoutPanelPrimary.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
			tableLayoutPanelPrimary.ColumnStyles.Clear(); 

			tableLayoutPanelVariat.ColumnCount = 0;
			tableLayoutPanelVariat.RowCount = 3;
			tableLayoutPanelVariat.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
			tableLayoutPanelVariat.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
			tableLayoutPanelVariat.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
			tableLayoutPanelVariat.ColumnStyles.Clear();

			tableLayoutPanelInterval.ColumnCount = 0;
			tableLayoutPanelInterval.RowCount = 4;
			tableLayoutPanelInterval.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
			tableLayoutPanelInterval.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
			tableLayoutPanelInterval.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
			tableLayoutPanelInterval.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
			tableLayoutPanelInterval.ColumnStyles.Clear();
			#endregion

			foreach (var val in Data)
			{
				var count = tableLayoutPanelPrimary.ColumnCount += 1;
				tableLayoutPanelPrimary.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 40F));
				tableLayoutPanelPrimary.Controls.Add(new Label() { Text = val.ToString() }, count - 1, 0);
			}

			var dataG = Data.GroupBy(x => x).OrderBy(g => g.Key);

			ValuesCharacteristics = dataG.ToDictionary(x => x.Key, x => new ValueCharacteristics (  x.Count(), (float)x.Count() / Data.Count()));

			foreach (var val in ValuesCharacteristics)
			{
				var count = tableLayoutPanelVariat.ColumnCount += 1;
				tableLayoutPanelVariat.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 40F));
				tableLayoutPanelVariat.Controls.Add(new Label() { Text = val.Key.ToString() }, count - 1, 0);
				tableLayoutPanelVariat.Controls.Add(new Label() { Text = val.Value.Frequency.ToString() }, count - 1, 1);
				tableLayoutPanelVariat.Controls.Add(new Label() { Text = val.Value.RelativeFrequency.ToString() }, count - 1, 2);
			}

			calculateResult(ValuesCharacteristics);
			calculateInterval();
		}

		void calculateResult(Dictionary<float, ValueCharacteristics> valuesCharacteristics)
		{
			var charts = Controls.OfType<Chart>().ToList();
			charts.ForEach(x => x.Series.ToList().ForEach(s => s.Points.Clear()));
			charts.ForEach(x => x.Refresh());

			foreach (var val in valuesCharacteristics)
			{
				float value = val.Key;
				int freq = val.Value.Frequency;
				float relFreq = val.Value.RelativeFrequency;

				chart1.Series["W [i]"].Points.AddXY(value.ToString("0.00"), freq);
				chart2.Series["W [i]/n"].Points.AddXY(value.ToString("0.00"), relFreq);

				//Построение графика функции распределения
				chart3.Series["F(x)"].Points.AddXY(Math.Round(value, 2), relFreq +
					valuesCharacteristics.Where(x => x.Key < value).Sum(x => x.Value.RelativeFrequency));
			}

			var Xv = valuesCharacteristics.Sum(x => x.Key * x.Value.Frequency) / Data.Count;
			labelXv.Text = "= " + Xv.ToString();

			var Dv = valuesCharacteristics.Sum(x => x.Value.Frequency * Math.Pow(x.Key - Xv, 2)) / Data.Count;
			labelDv.Text = "= " + Dv.ToString("0.0000");

			var Dfixed = Data.Count * Dv / (Data.Count - 1);
			labelDfixed.Text = "= " + Dfixed.ToString("0.0000");

			var DfixedSqrt = Math.Sqrt(Dfixed);
			labelDfixedSqrt.Text = "= " + DfixedSqrt.ToString("0.0000");
		}

		public struct ValueCharacteristics
		{
			public int Frequency;
			public float RelativeFrequency;

			public ValueCharacteristics(int frequency, float relativeFrequency)
			{
				Frequency = frequency;
				RelativeFrequency = relativeFrequency;
			}
		}

		void calculateInterval()
		{
			tableLayoutPanelInterval.Controls.Clear();
			tableLayoutPanelInterval.ColumnStyles.Clear();
			tableLayoutPanelInterval.ColumnCount = 0;

			var l = 0;
			int.TryParse(textBoxIntervalsCount.Text, out l);
			var h = (Values.Max() - Values.Min()) / l;

			Dictionary<float, ValueCharacteristics> valuesCharacteristics = new Dictionary<float, ValueCharacteristics>();

			float last = Values.Min();
			for (int i = 0; i < l; i++)
			{
				var limit1 = last;
				var limit2 = (float)Math.Round((limit1 + h), 4);
				int frequency = ValuesCharacteristics
					.Where(x => (x.Key == ValuesCharacteristics.First().Key ? limit1 <= x.Key : limit1 < x.Key) && x.Key <= limit2)
					.Sum(x => x.Value.Frequency);
				var relFreq = (float)frequency / Data.Count;
				var average = (limit1 + limit2) / 2;

				var count = tableLayoutPanelInterval.ColumnCount += 1;
				tableLayoutPanelInterval.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
				tableLayoutPanelInterval.Controls.Add(new Label() { Text = limit1.ToString("0.00") + " - " + (limit2).ToString("0.00")}, count - 1, 0);
				tableLayoutPanelInterval.Controls.Add(new Label() { Text = average.ToString() }, count - 1, 1);
				tableLayoutPanelInterval.Controls.Add(new Label() { Text = frequency.ToString() }, count - 1, 2);
				tableLayoutPanelInterval.Controls.Add(new Label() { Text = relFreq.ToString() }, count - 1, 3);

				//chart4.Series["W [i]"].Points.AddXY(average.ToString("0.00"), frequency);
				//chart5.Series["W [i]/n"].Points.AddXY(average.ToString("0.00"), relFreq);

				valuesCharacteristics[average] = new ValueCharacteristics(frequency, relFreq);

				last = limit2;
			}

			if (checkBoxBasedOnInterval.Checked)
			{
				calculateResult(valuesCharacteristics);
			}
		}

		private void chart3_PrePaint(object sender, ChartPaintEventArgs e)
		{
			chart3.Annotations.Clear();
			DataPoint previous = null;

			var points = chart3.Series["F(x)"].Points.OrderByDescending(p => p.XValue);

			labelFx.Text = "F(x) = { ";
			foreach (var point in points)
			{
				labelFx.Text += point.YValues[0].ToString("0.00") + " при " + (previous != null ? point.XValue.ToString("0.00") + " < x < " + previous.XValue.ToString("0.00") : "x > " + point.XValue.ToString("0.00")) + "\n";
				ArrowAnnotation arrow = new ArrowAnnotation();
				arrow.AnchorDataPoint = point;
				arrow.Height = 0;
				arrow.ArrowStyle = ArrowStyle.Simple;
				arrow.LineWidth = 1;
				arrow.ArrowSize = 3;
				arrow.Width = previous != null ? chart3.ChartAreas[0].AxisX.ValueToPosition(previous.XValue)
					- chart3.ChartAreas[0].AxisX.ValueToPosition(point.XValue)
					: 10;

				chart3.Annotations.Add(arrow);

				previous = point;
			}
			labelFx.Text += "0 при x <= " + (points.Any() ? points.Last().XValue.ToString("0.00") : "") + " }";
		}

		private void checkBoxBasedOnInterval_CheckedChanged(object sender, EventArgs e)
		{
			if (checkBoxBasedOnInterval.Checked)
				calculateInterval();
			else
				calculateResult(ValuesCharacteristics);
		}

		private void checkBoxCustomIntervals_CheckedChanged(object sender, EventArgs e)
		{

		}

		private void textBox_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)
				&& e.KeyChar != ',' && e.KeyChar != ' ' && e.KeyChar != ';' && e.KeyChar != '-')
			{
				e.Handled = true;
			}
		}

		private void textBoxIntervalsCount_TextChanged(object sender, EventArgs e)
		{
			calculateInterval();
		}
	}
}
