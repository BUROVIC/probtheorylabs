﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab6
{
	public partial class FormLab6 : Form
    {
        List<float> Data { get; set; }
        Dictionary<float, ValueCharacteristics> ValuesCharacteristics { get; set; }
        float nextVal(float key)
        {
            float val = ValuesCharacteristics.Where(k => k.Key > key).First().Key;
            return val;
        }
		public FormLab6()
		{
			InitializeComponent();

            var dataStr = Properties.Resources.Data.ToString();
            try
            {
                Data = dataStr.Split('\n').Select(float.Parse).ToList();
            }
            catch (Exception)
            {
                Data = dataStr.Replace('.', ',').Split('\n').Select(float.Parse).ToList();
            }

            tableLayoutPanelMain.ColumnCount = 0;
            tableLayoutPanelMain.RowCount = 1;
            tableLayoutPanelMain.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanelMain.ColumnStyles.Clear();

            tableLayoutPanelOrdered.ColumnCount = 0;
            tableLayoutPanelOrdered.RowCount = 3;
            tableLayoutPanelOrdered.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanelOrdered.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanelOrdered.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanelOrdered.ColumnStyles.Clear();

            foreach (var val in Data)
            {
                var count = tableLayoutPanelMain.ColumnCount += 1;
                tableLayoutPanelMain.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 40F));
                tableLayoutPanelMain.Controls.Add(new Label() { Text = val.ToString() }, count - 1, 0);
            }

            //<--------------------------------------------Ordered---------------------------------------------------
            var dataG = Data.GroupBy(x => x).OrderBy(g => g.Key);

            ValuesCharacteristics = dataG.ToDictionary(x => x.Key, x => new ValueCharacteristics(x.Count(), (float)x.Count() / Data.Count()));

            foreach (var val in ValuesCharacteristics)
            {
                float value = val.Key;
                int freq = val.Value.Frequency;
                float relFreq = val.Value.RelativeFrequency;

                var count = tableLayoutPanelOrdered.ColumnCount += 1;
                tableLayoutPanelOrdered.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 40F));
                tableLayoutPanelOrdered.Controls.Add(new Label() { Text = value.ToString() }, count - 1, 0);
                tableLayoutPanelOrdered.Controls.Add(new Label() { Text = freq.ToString() }, count - 1, 1);
                tableLayoutPanelOrdered.Controls.Add(new Label() { Text = relFreq.ToString() }, count - 1, 2);
            }

            var Xv = ValuesCharacteristics.Sum(x => x.Key * x.Value.Frequency) / Data.Count;
            labelXv.Text = "= " + Xv.ToString();

            var Dv = ValuesCharacteristics.Sum(x => x.Value.Frequency * Math.Pow(x.Key - Xv, 2)) / Data.Count;
            labelDv.Text = "= " + Dv.ToString("0.0000");

            var Sigma = Math.Sqrt(Dv);
            labelSigma.Text = "= " + Sigma.ToString("0.0000");
        }
        public struct ValueCharacteristics
        {
            public int Frequency;
            public float RelativeFrequency;

            public ValueCharacteristics(int frequency, float relativeFrequency)
            {
                Frequency = frequency;
                RelativeFrequency = relativeFrequency;
            }
        }
	}
}
