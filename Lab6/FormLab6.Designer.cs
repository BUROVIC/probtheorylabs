﻿namespace Lab6
{
	partial class FormLab6
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLab6));
            this.tableLayoutPanelOrdered = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelXv = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelDv = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.labelSigma = new System.Windows.Forms.Label();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanelOrdered
            // 
            this.tableLayoutPanelOrdered.ColumnCount = 1;
            this.tableLayoutPanelOrdered.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelOrdered.Location = new System.Drawing.Point(12, 95);
            this.tableLayoutPanelOrdered.Name = "tableLayoutPanelOrdered";
            this.tableLayoutPanelOrdered.RowCount = 3;
            this.tableLayoutPanelOrdered.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelOrdered.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelOrdered.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanelOrdered.Size = new System.Drawing.Size(476, 118);
            this.tableLayoutPanelOrdered.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(513, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(196, 57);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // labelXv
            // 
            this.labelXv.AutoSize = true;
            this.labelXv.Location = new System.Drawing.Point(717, 31);
            this.labelXv.Name = "labelXv";
            this.labelXv.Size = new System.Drawing.Size(42, 13);
            this.labelXv.TabIndex = 4;
            this.labelXv.Text = "labelXv";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(513, 76);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(391, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // labelDv
            // 
            this.labelDv.AutoSize = true;
            this.labelDv.Location = new System.Drawing.Point(911, 95);
            this.labelDv.Name = "labelDv";
            this.labelDv.Size = new System.Drawing.Size(43, 13);
            this.labelDv.TabIndex = 6;
            this.labelDv.Text = "labelDv";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(513, 137);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(419, 76);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 7;
            this.pictureBox3.TabStop = false;
            // 
            // labelSigma
            // 
            this.labelSigma.AutoSize = true;
            this.labelSigma.Location = new System.Drawing.Point(938, 164);
            this.labelSigma.Name = "labelSigma";
            this.labelSigma.Size = new System.Drawing.Size(58, 13);
            this.labelSigma.TabIndex = 8;
            this.labelSigma.Text = "labelSigma";
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.AutoScroll = true;
            this.tableLayoutPanelMain.AutoScrollMinSize = new System.Drawing.Size(99, 1);
            this.tableLayoutPanelMain.ColumnCount = 1;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(12, 21);
            this.tableLayoutPanelMain.MaximumSize = new System.Drawing.Size(600, 500);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 1;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(476, 35);
            this.tableLayoutPanelMain.TabIndex = 9;
            // 
            // FormLab6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(993, 405);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Controls.Add(this.labelSigma);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.labelDv);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.labelXv);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tableLayoutPanelOrdered);
            this.Name = "FormLab6";
            this.Text = "Лабораторная №6";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelOrdered;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelXv;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label labelDv;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label labelSigma;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;

    }
}

